package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Map;

public interface BankInterface {

    Map<Long, UserInterface> getUsers();
    void setUsers(Map<Long, UserInterface> users);
    void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException, WalletIsBlockedException, LimitExceededException, InsufficientWalletAmountException;
}
