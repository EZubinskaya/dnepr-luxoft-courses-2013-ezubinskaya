package com.luxoft.dnepr.courses.model;

public class Redis extends Entity {

    private Integer weight;

    public Integer getWeight() { return weight; }
    public void setWeight(Integer weight) { this.weight = weight; }
}