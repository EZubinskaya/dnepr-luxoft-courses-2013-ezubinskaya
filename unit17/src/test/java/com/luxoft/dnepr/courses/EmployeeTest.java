package com.luxoft.dnepr.courses;

import com.luxoft.dnepr.courses.dao.IDao;
import com.luxoft.dnepr.courses.model.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by ekaterina on 1/6/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
public class EmployeeTest {

    @Autowired
    private IDao<Employee> employeeDao;

    @Test
    public void saveTest() {

        Employee employee1 = new Employee();
        Employee employee2 = new Employee();

        employee1.setId(5);
        employee2.setId(6);

        employee1.setSalary(100);
        employee2.setSalary(200);

        int before = employeeDao.getAll().size();
        employeeDao.save(employee1);
        employeeDao.save(employee2);
        int after = employeeDao.getAll().size();

        Assert.assertEquals(before+2,after);
    }

    @Test
    public void editTest() {

        Employee employee1 = new Employee();
        employee1.setId(2);
        employee1.setSalary(2000);
        employeeDao.update(employee1);

        Assert.assertEquals((Integer) 2000,employeeDao.get(2).getSalary());
    }

    @Test
    public void deleteTest() {

        Employee employee1 = new Employee();
        employee1.setId(1);
        employee1.setSalary(280);
        employeeDao.update(employee1);

        int before = employeeDao.getAll().size();
        employeeDao.delete(1l);
        int after = employeeDao.getAll().size();

        Assert.assertEquals(before,after+1);
    }
}
