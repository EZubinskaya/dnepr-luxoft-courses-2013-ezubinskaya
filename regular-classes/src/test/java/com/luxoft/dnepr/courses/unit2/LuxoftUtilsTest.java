package com.luxoft.dnepr.courses.unit2;

import org.junit.*;
import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;

import java.util.ArrayList;
import java.util.List;

import static com.luxoft.dnepr.courses.unit2.LuxoftUtils.*;

public class LuxoftUtilsTest {
    @Test
    public void testSortArray() {
        Assert.assertArrayEquals(new String[]{"aloha", "aLooha", "Wita", "Anna"}, sortArray(new String[]{"Anna", "Wita", "aloha", "aLooha"}, false));
        Assert.assertArrayEquals(new String[]{"Anna", "Wita", "aLooha", "aloha"}, sortArray(new String[]{"Anna", "Wita", "aloha", "aLooha"}, true));
        Assert.assertArrayEquals(new String[]{"Hello", "Word", "caT", "cat", "word"}, sortArray(new String[]{"Hello", "word", "Word", "cat", "caT"}, true));
        Assert.assertArrayEquals(new String[]{"1", "2", "3", "4", "4"}, sortArray(new String[]{"1", "3", "2", "4", "4"}, true));
        Assert.assertArrayEquals(new String[]{"4", "4", "3", "2", "1"}, sortArray(new String[]{"1", "3", "2", "4", "4"}, false));
    }

    @Test
    public void testWordAverageLength() {
        Assert.assertEquals(2.25, wordAverageLength("I have a cat"), 0);
        Assert.assertEquals(2.75, wordAverageLength("I like a candy"), 0);
        Assert.assertEquals(3, wordAverageLength("222"), 0);
        Assert.assertEquals(4, wordAverageLength("null"), 0);
    }

    @Test
    public void testReverseWords() {
        Assert.assertEquals("   567", reverseWords("   765"));
        Assert.assertEquals("cba trf rgrg ", reverseWords("abc frt grgr "));
        Assert.assertEquals("anDy", reverseWords("yDna"));
        Assert.assertEquals("Ab  0A", reverseWords("bA  A0"));
    }

    @Test
    public void testGetCharEntries() throws Exception {
        Assert.assertArrayEquals(new char[]{'a', 'I', 'c', 'e', 'h', 't', 'v'}, getCharEntries("I have a cat"));
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("aaaAAA zzzSSS"));
        Assert.assertArrayEquals(new char[]{'a', 'P', 'p'}, LuxoftUtils.getCharEntries("aa Pp"));
    }

    private static final List<Figure> figures1 = new ArrayList<Figure>();
    private static final List<Figure> figures2 = new ArrayList<Figure>();
    private static final List<Figure> figures3 = new ArrayList<Figure>();
    private static final List<Figure> figures4 = new ArrayList<Figure>();

    @Before
    public void setUpCalculateOverallArea() {
        figures1.add(new Circle(3));
        figures1.add(new Square(4));
        figures1.add(new Hexagon(2));

        figures2.add(new Circle(2.15));
        figures2.add(new Square(44));
        figures2.add(new Hexagon(23));

        figures3.add(new Circle(0));
        figures3.add(new Square(0));
        figures3.add(new Hexagon(0));
    }

    @Test
    public void testCalculateOverallArea() throws Exception {
        Assert.assertEquals(54.666638, calculateOverallArea(figures1), 0.000001);
        Assert.assertEquals(3324.904327847123, calculateOverallArea(figures2), 0.000001);
        Assert.assertEquals(0, calculateOverallArea(figures3), 0.000001);
    }

    @Test(expected = RuntimeException.class)
    public void testCalculateOverallArea2() throws Exception {
        figures4.add(new Circle(-1));
        figures4.add(new Square(-1));
        figures4.add(new Hexagon(-1));
        calculateOverallArea(figures4);
    }

    @After
    public void tearDownCalculateOverallArea() {
        figures1.clear();
        figures2.clear();
        figures3.clear();
        figures4.clear();
    }

}
