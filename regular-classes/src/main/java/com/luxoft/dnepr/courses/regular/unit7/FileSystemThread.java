package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/22/13
 * Time: 11:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileSystemThread implements Callable<List<File>> {

    public final static String FILE_LIST_END = UUID.randomUUID().toString();

    protected LinkedBlockingQueue<String> queue;
    protected String rootFolder;

    protected List<File> scanFiles(String directoryName) throws InterruptedException {
        return scanFiles(new File(directoryName));
    }

    protected List<File> scanFiles(File file) throws InterruptedException {
        List<File> result = new ArrayList<File>();
        if (file != null) {
            if (file.isFile()) {
                if (file.getName().endsWith(".txt")) {
                    result.add(file);
                    queue.put(file.getAbsolutePath());
                }
            } else {
                File[] files = file.listFiles();
                for (File entity : files) {
                    result.addAll(scanFiles(entity));
                }
            }
        }
        return result;
    }

    public FileSystemThread(LinkedBlockingQueue<String> queue, String rootFolder) {
        this.queue = queue;
        this.rootFolder = rootFolder;
    }

    @Override
    public List<File> call() throws Exception {
        List<File> result = scanFiles(rootFolder);
        queue.put(FILE_LIST_END);
        return result;
    }
}
