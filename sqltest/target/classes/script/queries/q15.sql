SELECT makers.maker_id, AVG(screen) as avg_size 
FROM makers INNER JOIN product ON (makers.maker_id = product.maker_id) 
INNER JOIN laptop ON (laptop.model = product.model) GROUP BY maker_id
ORDER BY avg_size 