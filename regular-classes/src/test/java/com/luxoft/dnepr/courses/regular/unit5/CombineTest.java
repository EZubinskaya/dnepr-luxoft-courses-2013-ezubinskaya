package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.AbstractDAO;
import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.exception.WrongType;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/14/13
 * Time: 9:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class CombineTest {

    @Before
    public void empty(){
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
    }

    @Test(expected = UserAlreadyExist.class)
    public void save() throws UserAlreadyExist, WrongType {
        Redis rediska1 = new Redis();
        rediska1.setId(1l);
        rediska1.setWeight(100);

        Employee employee = new Employee();
        employee.setId(1l);
        employee.setSalary(120);

        AbstractDAO<Redis> a = new RedisDaoImpl();
        a.save(rediska1);

        AbstractDAO<Employee> b = new EmployeeDaoImpl();
        b.save(employee);
    }

    @Test (expected = WrongType.class)
    public void get() throws UserAlreadyExist,WrongType {
        Redis rediska1 = new Redis();
        rediska1.setId(1l);
        rediska1.setWeight(100);

        AbstractDAO<Redis> a = new RedisDaoImpl();
        a.save(rediska1);

        Employee employee = new Employee();
        employee.setId(2l);
        employee.setSalary(120);

        AbstractDAO<Employee> b = new EmployeeDaoImpl();
        b.save(employee);

        a.get(2l);
    }


    @Test
    public void get2() throws UserAlreadyExist, WrongType {
        Redis rediska1 = new Redis();
        rediska1.setId(1l);
        rediska1.setWeight(100);

        AbstractDAO<Redis> a = new RedisDaoImpl();
        a.save(rediska1);

        assertEquals(null,a.get(150l));
    }

    @Test (expected = WrongType.class)
    public void delete() throws UserAlreadyExist, WrongType {
        Redis rediska1 = new Redis();
        rediska1.setId(1l);
        rediska1.setWeight(100);

        Employee employee = new Employee();
        employee.setId(2l);
        employee.setSalary(120);

        AbstractDAO<Redis> a = new RedisDaoImpl();
        a.save(rediska1);

        AbstractDAO<Employee> b = new EmployeeDaoImpl();
        b.save(employee);

        a.delete(2l);
    }

    @Test( expected = WrongType.class)

    public void update() throws UserNotFound, WrongType {
        Employee employee = new Employee();
        employee.setId(1l);
        employee.setSalary(120);
        AbstractDAO<Employee> ado = new EmployeeDaoImpl();
        try {
            ado.save(employee);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }

        Redis rediska1 = new Redis();
        rediska1.setId(1l);
        rediska1.setWeight(100);
        AbstractDAO<Redis> adr = new RedisDaoImpl();
        try {
            adr.update(rediska1);
        } catch (UserNotFound ex) {
            ex.printStackTrace();
        }
    }
}
