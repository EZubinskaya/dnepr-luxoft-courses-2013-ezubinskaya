package com.luxoft.dnepr.courses;

import com.luxoft.dnepr.courses.dao.IDao;
import com.luxoft.dnepr.courses.model.Employee;
import com.luxoft.dnepr.courses.model.Redis;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by ekaterina on 1/6/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
public class RedisTest {

    @Autowired
    private IDao<Redis> redisDao;

    @Test
    public void saveTest() {

        Redis redis1 = new Redis();
        Redis redis2 = new Redis();

        redis1.setId(5);
        redis2.setId(6);

        redis1.setWeight(10);
        redis2.setWeight(20);

        int before = redisDao.getAll().size();
        redisDao.save(redis1);
        redisDao.save(redis2);
        int after = redisDao.getAll().size();

        Assert.assertEquals(before + 2, after);
    }

    @Test
    public void editTest() {

        Redis redis1 = new Redis();
        redis1.setId(2);
        redis1.setWeight(170);
        redisDao.update(redis1);

        Assert.assertEquals((Integer) 170, redisDao.get(2).getWeight());
    }

    @Test
    public void deleteTest() {

        Redis redis1 = new Redis();
        redis1.setId(1);
        redis1.setWeight(280);
        redisDao.update(redis1);

        int before = redisDao.getAll().size();
        redisDao.delete(1l);
        int after = redisDao.getAll().size();

        Assert.assertEquals(before,after+1);
    }
}
