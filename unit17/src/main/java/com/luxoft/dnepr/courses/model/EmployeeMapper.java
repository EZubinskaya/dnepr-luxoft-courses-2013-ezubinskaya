package com.luxoft.dnepr.courses.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ekaterina on 1/6/14.
 */
public class EmployeeMapper implements RowMapper<Employee> {

    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {

        Employee employee = new Employee();
        employee.setId(rs.getInt("id"));
        employee.setSalary(rs.getInt("salary"));
        return employee;
    }
}
