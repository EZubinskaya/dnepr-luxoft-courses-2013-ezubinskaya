package com.luxoft.dnepr.courses.regular.unit14;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class Vocabulary {

    private List<String> vocabulary;

    protected Integer wordsCount = null;

    public Vocabulary(String fileName) {
        StringBuilder stringBuilder = readFile(fileName);
        vocabulary = parseFile(stringBuilder);
    }

    protected  StringBuilder readFile(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        InputStream inputStream = getClass().getResourceAsStream("/"+fileName);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException ex) {
            ex.getStackTrace();
        }
        return stringBuilder;
    }

    protected List<String> parseFile(StringBuilder text) {
        Set<String> tempVocabulary = new HashSet<String>();
        String str=text.toString().replaceAll("\\W"," ");
        StringTokenizer t=new StringTokenizer(str);
        while(t.hasMoreTokens()){
            String to = t.nextToken();
            if(to.length()>3) {
                tempVocabulary.add(to.toLowerCase());
            }
        }
        wordsCount = tempVocabulary.size();
        return new ArrayList(tempVocabulary);
    }

    public Integer getWordsCount() {
        return wordsCount;
    }

    public String getRandomWord() {
        Random random = new Random();
        int wordIndex = random.nextInt(vocabulary.size());
        return vocabulary.remove(wordIndex);
    }
}
