package com.luxoft.dnepr.courses.regular.unit12;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/11/13
 * Time: 10:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class UserServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        RequestDispatcher dispatcher =
                request.getRequestDispatcher("user.jsp");
        dispatcher.forward( request, response );
    }

    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException {

        HttpSession session = request.getSession(false);
        if(session != null){
            session.invalidate();
        }
        response.sendRedirect("/index.html");
    }
}
