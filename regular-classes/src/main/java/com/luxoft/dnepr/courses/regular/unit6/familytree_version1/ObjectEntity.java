package com.luxoft.dnepr.courses.regular.unit6.familytree_version1;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/17/13
 * Time: 2:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectEntity {
    String name = null;
    String className;
    Long version;
    ArrayList<ObjectField> fields = new ArrayList<ObjectField>();
    ArrayList<ObjectEntity> entities = new ArrayList<ObjectEntity>();
}
