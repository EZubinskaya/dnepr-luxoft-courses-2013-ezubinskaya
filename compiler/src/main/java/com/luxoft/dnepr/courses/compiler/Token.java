package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Cooper
 * Date: 27.10.13
 * Time: 10:55
 * To change this template use File | Settings | File Templates.
 */
public class Token implements Comparable<Token>{

    private Operator operator;
    private int priority;
    ByteArrayOutputStream stack;
    public double value;

    Token(Double value, Operator operator, int priority)  {
        this.operator = operator;
        this.priority = priority;
        this.value = value;
        stack = new ByteArrayOutputStream();
        Compiler.addCommand(stack, VirtualMachine.PUSH, value);
    }

    ByteArrayOutputStream getStack() {
        return stack;
    }

    int getPriority() {
        return priority;
    }

    Operator getOperator() {
        return operator;
    }
    @Override
    public int compareTo(Token o) {
        return this.priority - ((Token)o).getPriority();
    }

    public void mergeToken(Token token) {
        try {
            stack.write(token.getStack().toByteArray());
            Compiler.addCommand(stack, token.getOperator().getCode());
        } catch (IOException ex) {
            throw new CompilationException("Cannot write to output stream");
        }
    }

    public Token prepareForOutput() {
        stack.write(VirtualMachine.PRINT);
        return this;
    }

}
