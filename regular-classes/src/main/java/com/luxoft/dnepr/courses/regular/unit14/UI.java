package com.luxoft.dnepr.courses.regular.unit14;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;


public class UI {
    private Vocabulary vocabulary;
    private int knownWordsCount;
    private int unknownWordsCount;

    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }

    public void run() {
        if (init()) {
            interact();
        }
    }

    protected Boolean init() {
        vocabulary = new Vocabulary("sonnets.txt");
        if (vocabulary.getWordsCount() == 0) {
            System.out.println("Sorry, vocabulary is empty, we can't start the test");
            return false;
        } else {
            return true;
        }
    }

    protected void interact() {
        Scanner s = new Scanner(System.in);
        Status status = Status.NEXT_WORD;
        while(status == Status.NEXT_WORD) {
            System.out.println(vocabulary.getRandomWord());
            System.out.println("Do you know translation of this word?:");
            do {
                String answer = s.next();
                status = processAnswer(answer);
            } while(status == Status.WAIT_ANSWER);
        }
        attitude();
    }

    private Status processAnswer(String answer) {
        switch(answer.toLowerCase()) {
            case "exit" :
                return Status.EXIT_TEST;
            case "help" : System.out.println("Lingustic analizator v1, " +
                    "autor Tushar Brahmacobalol for help type help, answer " +
                    "y/n question, your english knowlege will give you");
                return Status.WAIT_ANSWER;
            case "y" :
                knownWordsCount++;
                return Status.NEXT_WORD;
            case "n" :
                unknownWordsCount++;
                return Status.NEXT_WORD;
            default : System.out.println("Error while reading answer");
                return Status.WAIT_ANSWER;
        }
    }

    void attitude() {
        if(knownWordsCount==0 && unknownWordsCount==0) {
            System.out.println("Sorry we can't attitude your vocabulary size, \'cause you\'d not answer any question");
            return;
        }
        int estimatedWordsCount = vocabulary.getWordsCount() * knownWordsCount / (knownWordsCount + unknownWordsCount);
        System.out.println("Your estimated vocabulary is " + estimatedWordsCount + " words");
    }

}
