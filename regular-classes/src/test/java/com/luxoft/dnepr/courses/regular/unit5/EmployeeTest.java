package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.exception.WrongType;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import junit.framework.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/13/13
 * Time: 8:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmployeeTest {
    IDao<Employee> dao = new EmployeeDaoImpl();
    Employee employee1;
    Employee employee2;

    @Test
    public void testSave1() {
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        try {
            assertEquals(employee1, dao.save(employee1));
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testSave2() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
    Assert.assertEquals(1,EntityStorage.getEntities().size());
    }

    @Test
    public void testSave4() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
        try {
            Assert.assertEquals(dao.get(1l),EntityStorage.getEntities().get(1l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test(expected = UserAlreadyExist.class)
    public void testSave3() throws UserAlreadyExist {
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        employee2 = new Employee();
        employee2.setId(1l);
        employee2.setSalary(225);
        dao.save(employee1);
        dao.save(employee2);
    }

    @Test
    public void testUpdae1() throws UserNotFound, UserAlreadyExist {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        employee2 = new Employee();
        employee2.setId(1l);
        employee2.setSalary(225);
        dao.save(employee1);
        try {
            dao.update(employee2);
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

        Assert.assertEquals(EntityStorage.getEntities().get(1l),employee2);
    }

    @Test(expected = UserNotFound.class)
    public void testUpdae2() throws UserNotFound, UserAlreadyExist {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        employee2 = new Employee();
        employee2.setId(175l);
        employee2.setSalary(225);
        dao.save(employee1);
        try {
            dao.update(employee2);
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test(expected = UserNotFound.class)
    public void testUpdae3() throws UserNotFound, UserAlreadyExist {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        employee2 = new Employee();
        employee2.setId(null);
        employee2.setSalary(225);
        dao.save(employee1);
        try {
            dao.update(employee2);
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test
    public void testGet1() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
        try {
            assertEquals(employee1,dao.get(1l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test
    public void testGet2() {
        try {
            assertEquals(null,dao.get(100l));
        } catch(WrongType ex) {

        }

    }

    @Test
    public void testDelete1() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Employee();
        employee1.setId(1l);
        employee1.setSalary(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
        try {
            assertEquals(true, dao.delete(1l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testDelete2() {
        try {
            assertEquals(false,dao.delete(100l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }
    }
}

