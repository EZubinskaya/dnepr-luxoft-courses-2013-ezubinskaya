package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Redis;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/13/13
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class RedisDaoImpl extends AbstractDAO<Redis> {

    public RedisDaoImpl() {
        super(Redis.class);
    }
}
