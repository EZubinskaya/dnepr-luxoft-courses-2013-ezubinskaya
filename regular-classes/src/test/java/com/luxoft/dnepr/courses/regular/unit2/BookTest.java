package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
        assertEquals(book,cloned);
        cloned.setPublicationDate(new GregorianCalendar(2006, 0, 2).getTime());
        assertNotSame(book,cloned);
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book2 = productFactory.createBook("code", "Thinking in Java", 230, new GregorianCalendar(2006, 0, 1).getTime());
        assertEquals(true, book.equals(book2));

        Book book3 = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book book4 = productFactory.createBook("code", "Thinking in Java2", 230, new GregorianCalendar(2006, 0, 1).getTime());
        assertEquals(false, book3.equals(book4));
    }
}
