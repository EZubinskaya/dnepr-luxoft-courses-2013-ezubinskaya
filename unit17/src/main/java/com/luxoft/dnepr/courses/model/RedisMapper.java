package com.luxoft.dnepr.courses.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ekaterina on 1/6/14.
 */
public class RedisMapper implements RowMapper<Redis>{

    public Redis mapRow(ResultSet rs, int rowNum) throws SQLException {

        Redis redis = new Redis();
        redis.setId(rs.getInt("id"));
        redis.setWeight(rs.getInt("weight"));
        return redis;
    }

}
