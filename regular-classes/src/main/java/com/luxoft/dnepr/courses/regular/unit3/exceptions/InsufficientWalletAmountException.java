package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class InsufficientWalletAmountException extends Exception {

    private Long walletId;
    private BigDecimal amountToWithdraw;
    private BigDecimal amountInWallet;

    public InsufficientWalletAmountException(Long walletId, BigDecimal amountToWithdraw, BigDecimal amountInWallet, String message) {
        super(message);
        this.walletId = walletId;
        this.amountToWithdraw = amountToWithdraw;
        this.amountInWallet = amountInWallet;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAmountToWithdraw() {
        return amountToWithdraw;
    }

    public BigDecimal getAmountWallet() {
        return amountInWallet;
    }
}
