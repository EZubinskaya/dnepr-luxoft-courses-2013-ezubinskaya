package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Calendar;
import java.util.Date;

/**
 * Product factory.
 * Simplifies creation of different kinds of products.
 */
public class ProductFactory {

    public Bread createBread(String code, String name, double price,
                             double weight) {
        Bread currentBread = new Bread();
        currentBread.setCode(code);
        currentBread.setName(name);
        currentBread.setPrice(price);
        currentBread.setweight(weight);
        return currentBread;
    }

    public Beverage createBeverage(String code, String name, double price,
                                   boolean nonAlcoholic) {
        Beverage currentBeverage = new Beverage();
        currentBeverage.setCode(code);
        currentBeverage.setName(name);
        currentBeverage.setNonAlcoholic(nonAlcoholic);
        currentBeverage.setPrice(price);
        return currentBeverage;
    }

    public Book createBook(String code, String name, double price,
                           Date publicationDate) {
        Book currentBook = new Book();
        currentBook.setCode(code);
        currentBook.setName(name);
        currentBook.setPrice(price);
        currentBook.setPublicationDate(publicationDate);
        return currentBook;
    }
}
