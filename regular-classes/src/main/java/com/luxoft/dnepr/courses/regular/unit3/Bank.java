package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/6/13
 * Time: 9:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class Bank implements  BankInterface{

    public Bank(String expectedJavaVersion) {
        if(!System.getProperty("java.version").equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(System.getProperty("java.home"),  expectedJavaVersion,"IllegalVersion");
        }
    }

    private Map<Long, UserInterface> users;

    public Map<Long, UserInterface> getUsers() {
        return  users;
    }

    public UserInterface getUserById(Long id) throws NoUserFoundException {
        if(!users.containsKey(id) ) {
            throw new NoUserFoundException(id, "No user with id " + id);
        }
        return users.get(id);
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException, WalletIsBlockedException, LimitExceededException, InsufficientWalletAmountException {
        UserInterface user1 =getUserById(fromUserId);
        WalletInterface wallet1 = user1.getWallet();
        UserInterface user2 =  getUserById(toUserId);
        WalletInterface wallet2 = user2.getWallet();

        try {
            try {
                wallet1.checkWithdrawal(amount);
            } catch(InsufficientWalletAmountException ex) {}
            try {
                wallet2.checkTransfer(amount);
            } catch(LimitExceededException ex) {}
            wallet1.checkWithdrawal(amount);
            wallet2.checkTransfer(amount);
        } catch (WalletIsBlockedException | InsufficientWalletAmountException | LimitExceededException ex) {
            throw new TransactionException(ex.getMessage());
        }

        wallet1.withdraw(amount);
        wallet2.transfer(amount);
    }
}


