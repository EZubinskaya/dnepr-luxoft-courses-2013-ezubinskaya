package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

import static com.luxoft.dnepr.courses.unit1.LuxoftUtils.*;

public class LuxoftUtilsTest {
    // Third task test
    @Test
    public void BinaryTodecimalTest() {
        Assert.assertEquals("20", binaryTodecimal("10100"));
        Assert.assertEquals("4", binaryTodecimal("100"));
        Assert.assertEquals("Not binary", binaryTodecimal("2a"));
        Assert.assertEquals("1", binaryTodecimal("1"));
    }

    // Four task test
    @Test
    public void decimalToBinaryTest() {

        Assert.assertEquals("1000011", decimalToBinary("67"));
        Assert.assertEquals("1", decimalToBinary("1"));
        Assert.assertEquals("Not decimal", decimalToBinary("A67"));
        Assert.assertEquals("Not decimal", decimalToBinary(" "));
        Assert.assertEquals("Not decimal", decimalToBinary("1 "));
    }

    // Five task test
    @Test
    public void sortArrayTest() {

        Assert.assertArrayEquals(new int[]{0, 1, 2}, sortArray(new int[]{1, 0, 2}, true));
        Assert.assertArrayEquals(new int[]{0, 1, 2, 67, 92}, sortArray(new int[]{92, 1, 67, 0, 2}, true));
        Assert.assertArrayEquals(new int[]{92, 75, 45}, sortArray(new int[]{92, 45, 75}, false));
        Assert.assertArrayEquals(new int[]{92, 75, 45}, sortArray(new int[]{92, 45, 75}, false));
    }

    // Second task
    @Test
    public void getMonthNameTest() {
        Assert.assertEquals("January", getMonthName(1, "en"));
        Assert.assertEquals("Декабрь", getMonthName(12, "ru"));
        Assert.assertEquals("Unknown Language", getMonthName(5, "hindi"));
        Assert.assertEquals("Unknown Language", getMonthName(13, "hindi"));
        Assert.assertEquals("Неизвестный месяц", getMonthName(-5, "ru"));

    }
}