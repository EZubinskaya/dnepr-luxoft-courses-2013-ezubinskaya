package com.luxoft.dnepr.courses.regular.unit6.familytree.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree.impl.PersonImpl;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Scanner;

public class IOUtils {
	
	private IOUtils() {
	}
	
	public static FamilyTree load(String filename) throws FileNotFoundException, PersonImpl.PersonJSONParseException {
        InputStream is = new FileInputStream(filename);
        return load(is);
	}
	
	public static FamilyTree load(InputStream is) throws PersonImpl.PersonJSONParseException {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return FamilyTreeImpl.create(PersonImpl.readPerson(s.hasNext() ? s.next() : ""));
	}
	
	public static void save(String filename, FamilyTree familyTree) throws IOException {
        OutputStream os = new FileOutputStream(filename, false);
        save(os, familyTree);
	}
	
	public static void save(OutputStream os, FamilyTree familyTree) throws IOException {
        if (!(familyTree instanceof FamilyTreeImpl));
        String res = ((PersonImpl) familyTree.getRoot()).writePerson();
        os.write(((PersonImpl) familyTree.getRoot()).writePerson().getBytes(Charset.forName("UTF-8")));
    }
}
