package com.luxoft.dnepr.courses.regular.unit6.familytree_version1;

import java.io.Serializable;

public interface FamilyTree extends Serializable {
	
	Person getRoot();
	long getCreationTime();
}
