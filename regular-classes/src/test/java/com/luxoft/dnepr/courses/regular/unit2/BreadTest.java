package com.luxoft.dnepr.courses.regular.unit2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.*;
import org.junit.Test;
/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/3/13
 * Time: 11:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class BreadTest {

    @Test
    public void testEquals() throws Exception {
        ProductFactory   productFactory = new ProductFactory();
        Bread b1 = productFactory. createBread("code","white",3,1);
        Bread b2 = productFactory. createBread("code","white",4,1);
        Bread b3 = productFactory. createBread("code","black",4,1);
        assertEquals(true,b1.equals(b2))  ;
        assertEquals(false,b1.equals(b3))  ;
    }
}
