package com.luxoft.dnepr.courses.unit3.view;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;
import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class CommandLineInterface {

    private Scanner scanner;
    private PrintStream output;

    public CommandLineInterface(PrintStream output, InputStream input) {
        this.scanner = new Scanner(input);
        this.output = output;
    }

    public void play() {
        output.println("Console Blackjack application.\n" +
                "Author: **Cooper**\n" +
                "(C) Luxoft 2013\n");

        GameController controller = GameController.getInstance();
        controller.newGame();
        output.println();
        printState(controller);
        while (scanner.hasNext()) {
            String command = scanner.next();
            if (!execute(command, controller)) {
                return;
            }
        }
    }

    /**
     * Выполняем команду, переданную с консоли. Список разрешенных комманд можно найти в классе {@link Command}.
     * Используйте методы контроллера чтобы обращаться к логике игры. Этот класс должен содержать только интерфейс.
     * Если этот метод вернет false - игра завершится.
     * <p/>
     * Более детальное описание формата печати можно узнать посмотрев код юниттестов.
     *
     * @see //com. luxoft. dnepr. courses. unit3. view. CommandLineInterfaceTest
     *      <p/>
     *      Описание команд:
     *      Command.HELP - печатает помощь.
     *      Command.MORE - еще одну карту и напечатать Состояние (GameController.requestMore())
     *      если после карты игрок проиграл - напечатать финальное сообщение и выйти
     *      Command.STOP - игрок закончил, теперь играет диллер (GameController.requestStop())
     *      после того как диллер сыграл напечатать:
     *      Dealer turn:
     *      пустая строка
     *      состояние
     *      пустая строка
     *      финальное сообщение
     *      Command.EXIT - выйти из игры
     *      <p/>
     *      Состояние:
     *      рука игрока (total вес)
     *      рука диллера (total вес)
     *      <p/>
     *      например:
     *      3 J 8 (total 21)
     *      A (total 11)
     *      <p/>
     *      Финальное сообщение:
     *      В зависимости от состояния печатаем:
     *      Congrats! You win!
     *      Push. Everybody has equal amount of points.
     *      Sorry, today is not your day. You loose.
     *      <p/>
     *      Постарайтесь уделить внимание чистоте кода и разделите этот метод на несколько подметодов.
     */

    private boolean execute(String command, GameController controller) {
        List<Card> myHand = controller.getMyHand();
        switch (command) {
            case Command.HELP:
                output.println("Usage: \n" +
                        "\thelp - prints this message\n" +
                        "\thit - requests one more card\n" +
                        "\tstand - I'm done - lets finish\n" +
                        "\texit - exits game");
                break;
            case Command.MORE:
                controller.requestMore();
                printState(controller);
                return message(controller, false);
            case Command.STOP:
                output.println("Dealer turn:\n");
                controller.requestStop();
                printState(controller);
                message(controller, true);
                return false;
            case Command.EXIT:
                return false;
            default:
                output.println("Invalid command");
        }
        return true;
    }

    private void printState(GameController controller) {
        List<Card> myHand = controller.getMyHand();
        format(myHand);
        List<Card> dealersHand = controller.getDealersHand();
        format(dealersHand);
    }

    private boolean message(GameController controller, boolean forceEndGame) {
        if (!forceEndGame) {
            int myScore = Deck.costOf(controller.getMyHand());
            if (myScore <= GameController.VICTORY_SCORE) {
                return true;
            }
        }
        if (controller.getWinState() == WinState.WIN) {
            output.println("\nCongrats! You win!");
        } else if (controller.getWinState() == WinState.LOOSE) {
            output.println("\nSorry, today is not your day. You loose.");
        } else {
            output.println("\nPush. Everybody has equal amount of points.");
        }
        return false;
    }

    private void format(List<Card> hand) {
        int cost = Deck.costOf(hand);
        for (Card card : hand) {
            output.print(card.getRank().getName() + " ");
        }
        output.print("(total " + cost + ")");
        output.print("\n");
    }
}
