package com.luxoft.dnepr.courses.regular.unit2;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/3/13
 * Time: 10:04 PM
 * To change this template use File | Settings | File Templates.
 */
public enum Discount {

    DISCOUNT_2(2,0.05),
    DISCOUNT_3(3,0.1);

    private int amount;
    private double percent;

    static Integer maxAmount;

    Discount(int amount, double percent) {
        this.amount = amount;
        this.percent = percent;
    }

    public int getAmount() {
        return amount;
    }

    public double getPercent() {
        return percent;
    }

    static double getPrice(double price, int amount) {
        //Вычисляем максимальное количество товаров, для которых задана определенная скидка
        //и записываем ее в стат. переменную для повторного использования
        if (maxAmount == null) {
            maxAmount = 0;
            for (Discount d : Discount.values()) {
                maxAmount = Math.max(maxAmount, d.getAmount());
            }
        }
        //Если количество товара превысило максимальное количество, для которых определена скидка,
        //то используем это количество для поиска скидки
        amount = Math.min(amount, maxAmount);
        double discountMult = 1;
        for (Discount d : Discount.values()) {
            if (d.getAmount() == amount) {
                discountMult -= d.getPercent();
                break;
            }
        }
        return price * discountMult;
    }

}