package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/10/13
 * Time: 10:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class EqualSet<E> implements Set<E> {

    private Object[] elementData;
    private int size;
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    static final int DEFAULT_INITIAL_CAPACITY = 16;
    int threshold;
    final float loadFactor = DEFAULT_LOAD_FACTOR;
    protected int modCount = 0;


    public EqualSet(Collection<? extends E> c) {
        if (c.size() < 0)
            throw new IllegalArgumentException("Illegal Capacity: "+
                    c.size());
        this.elementData = new Object[(Math.max((int) (c.size()/.75f) + 1, 16))];
    }

    public EqualSet() {
        threshold = (int)(DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
        this.elementData = new Object[threshold];
    }
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) >= 0;
    }

    private int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++)
                if (elementData[i]==null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (o.equals(elementData[i]))
                    return i;
        }
        return -1;
    }

    @Override
    public Iterator<E> iterator() throws NoSuchElementException, IllegalStateException{
        return new Iterator<E>() {
            int cursor;       // index of next element to return

            @Override
            public boolean hasNext() {
                return cursor < size;
            }
            @Override
            public E next() {
                if (cursor >= size)
                    throw new NoSuchElementException();
                Object result = elementData[cursor];
                cursor = cursor + 1;
                return (E)result;
            }

            @Override
            public void remove() {
                if (cursor < cursor - 1 && cursor > 0) {
                    System.arraycopy(size, cursor + 1,
                            size, cursor, cursor - cursor - 1);
                } else {
                    throw new IllegalStateException();
                }
                cursor--;
            }
        } ;
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elementData, size);
    }
  //!!!!!!!!!!!!!!!!!!!!!!
    @Override
    public <T> T[] toArray(T[] a) throws NullPointerException {
        if(a == null) {
            throw new NullPointerException();
        }
        if (a.length < size)
            // Make a new array of a's runtime type, but my contents:
            return (T[]) Arrays.copyOf(elementData, size, a.getClass());
        System.arraycopy(elementData, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    @Override
    public boolean add(E e) {
        if(contains(e)) {
            return false;
        }
        ensureCapacity(size + 1);  // Increments modCount!!
        elementData[size++] = e;
        return true;
    }

    private void ensureCapacity(int minCapacity) {
        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            Object oldData[] = elementData;
            int newCapacity = (oldCapacity * 3)/2 + 1;
            if (newCapacity < minCapacity)
                newCapacity = minCapacity;
            // minCapacity is usually close to size, so this is a win:
            elementData = Arrays.copyOf(elementData, newCapacity);
        }
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (int index = 0; index < size; index++)
                if (elementData[index] == null) {
                    fastRemove(index);
                    return true;
                }
        } else {
            for (int index = 0; index < size; index++)
                if (o.equals(elementData[index])) {
                    fastRemove(index);
                    return true;
                }
        }
        return false;
    }

    private void fastRemove(int index) {
        modCount++;
        int numMoved = size - index - 1;
        if (numMoved > 0)
            System.arraycopy(elementData, index+1, elementData, index,
                    numMoved);
        elementData[--size] = null; // Let gc do its work
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        for(Object o : c) {
            if(contains(c)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if(c.equals(null)) {
            throw new NullPointerException();
        }
        int calc =0;
        for(Object o : c) {
            if(null == c) {
                calc++;
                if(calc>1){
                    throw new NullPointerException();
                }
            }
        }
        Object[] a = c.toArray();
        int numNew = a.length;
        ensureCapacity(size + numNew);  // Increments modCount
        System.arraycopy(a, 0, elementData, size, numNew);
        size += numNew;
        return numNew != 0;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return batchRemove(c, true);
    }

    private boolean batchRemove(Collection<?> c, boolean complement) {
        final Object[] elementData = this.elementData;
        int r = 0, w = 0;
        boolean modified = false;
        try {
            for (; r < size; r++)
                if (c.contains(elementData[r]) == complement)
                    elementData[w++] = elementData[r];
        } finally {
            // Preserve behavioral compatibility with AbstractCollection,
            // even if c.contains() throws.
            if (r != size) {
                System.arraycopy(elementData, r,
                        elementData, w,
                        size - r);
                w += size - r;
            }
            if (w != size) {
                for (int i = w; i < size; i++)
                    elementData[i] = null;
                modCount += size - w;
                size = w;
                modified = true;
            }
        }
        return modified;
    }


    @Override
    public boolean removeAll(Collection<?> c) {
        return batchRemove(c, false);
    }

    @Override
    public void clear() {
        modCount++;
        for (int i = 0; i < size; i++)
            elementData[i] = null;
        size = 0;
    }


}
