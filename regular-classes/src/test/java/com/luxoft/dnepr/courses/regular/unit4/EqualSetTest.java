package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Test;

import java.util.Iterator;

import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/10/13
 * Time: 11:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {

    @Test
    public void addNullTest() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add(null);
        assertFalse(equalSet.add(null));
    }

    @Test
    public void addTest1() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add("First");
        equalSet.add("Second");
        assertFalse(equalSet.add("First"));
    }

    @Test(expected = NullPointerException.class)
    public void addTest2() {
        Set<String> equalSet = new EqualSet<>();
        equalSet.add("First");
        equalSet.add("Second");
        equalSet.addAll(null);

    }

    @Test
    public void addTest3() {
        EqualSet<String> testSet = new EqualSet<>();
        assertTrue(testSet.add("a"));
    }

    @Test
    public void addTest4() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        assertFalse(testSet.add("a"));
    }

    @Test
    public void addTest5() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        testSet.add("b");
        testSet.add("c");
        assertFalse(testSet.add("c"));
    }

    @Test
    public void containsTest1() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        assertTrue(testSet.contains("a"));
    }

    @Test
    public void containsTest2() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        assertFalse(testSet.contains("b"));
    }

    @Test
    public void containsTest3() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add(null);
        assertTrue(testSet.contains(null));
    }

    @Test
    public void emptyTest1() {
        EqualSet<String> testSet = new EqualSet<>();
        assertTrue(testSet.isEmpty());
    }

    @Test
    public void emptyTest2() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        assertFalse(testSet.isEmpty());
    }

    @Test
    public void sizeTest1() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("a");
        assertEquals(1,testSet.size());
    }

    @Test
    public void sizeTest2() {
        EqualSet<String> testSet = new EqualSet<>();
        assertEquals(0,testSet.size());
    }

    @Test
    public void objectToArrayTest1() {
        EqualSet<String> testSet = new EqualSet<>();
        testSet.add("A");
        testSet.add("B");
        testSet.add("C");
        assertArrayEquals(new Object[]{"A", "B", "C"},testSet.toArray());
    }

    @Test
    public void toArrayTest1(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);
        Integer[] t = new Integer [4];
        t[0]=10;
        t[1]=12;
        t[2]=31;
        t[3]=49;
        Integer list2[] = new Integer[arrlist.size()];
        list2 = arrlist.toArray(list2);

        assertArrayEquals(t, list2);
    }

    @Test (expected = java.lang.ArrayStoreException.class)
    public void toArrayTest2(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        Integer[] t = new Integer [4];
        t[0]=10;
        t[1]=12;
        t[2]=31;
        t[3]=49;

        String list2[] = new String[arrlist.size()];
        list2 = arrlist.toArray(list2);

    }

    @Test (expected = java.lang.NullPointerException.class)
    public void toArrayTest3(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        Integer[] t = null;

        arrlist.toArray(t);

    }

    @Test
    public void reteinAllTest1(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        EqualSet<Integer> arrlist2 = new EqualSet<Integer>();
        arrlist2.add(10);
        arrlist2.add(12);
        arrlist2.add(31);

        arrlist.retainAll(arrlist2);
        assertEquals(3,arrlist.size());
    }

    @Test
    public void reteinAllTest2(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        EqualSet<Integer> arrlist2 = new EqualSet<Integer>();
        arrlist2.add(10);
        arrlist2.add(12);
        arrlist2.add(31);

        assertTrue(arrlist.retainAll(arrlist2));
    }

    @Test
    public void removeAllTest1(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        EqualSet<Integer> arrlist2 = new EqualSet<Integer>();
        arrlist2.add(10);
        arrlist2.add(12);
        arrlist2.add(31);

        assertTrue(arrlist.removeAll(arrlist2));
        assertEquals(1, arrlist.size());
    }

    @Test
    public void removeAllTest2(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        EqualSet<Integer> arrlist2 = new EqualSet<Integer>();
        arrlist2.add(10);
        arrlist2.add(12);
        arrlist2.add(31);

        arrlist.removeAll(arrlist2);
        assertTrue(arrlist.contains(49));
    }

    @Test
    public void removeAllTest3(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        EqualSet<Integer> arrlist2 = new EqualSet<Integer>();
        arrlist2.add(10);
        arrlist2.add(12);
        arrlist2.add(31);

        arrlist.removeAll(arrlist2);
        assertFalse(arrlist.contains(10));
    }

    @Test
    public void clearTest(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(10);
        arrlist.add(12);
        arrlist.add(31);
        arrlist.add(49);

        arrlist.clear();
        assertTrue(arrlist.isEmpty());
    }

    @Test
    public void iteratorTest1(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(1);
        arrlist.add(2);
        arrlist.add(3);
        arrlist.add(4);
        Integer t = 1;

        Iterator<Integer> i = arrlist.iterator();
        while(i.hasNext()) {
            assertEquals(t,i.next());
            t++;
        }
    }

    @Test (expected = java.util.NoSuchElementException.class)
    public void iteratorTest2(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        arrlist.add(1);
        arrlist.add(2);
        arrlist.add(3);
        arrlist.add(4);
        Integer t = 1;

        Iterator<Integer> i = arrlist.iterator();
        while(i.hasNext()) {
            assertEquals(t,i.next());
            t++;
        }
        i.next();
    }

    @Test (expected = IllegalStateException.class)
    public void iteratorTest3(){
        EqualSet<Integer> arrlist = new EqualSet<Integer>();
        Integer t = 1;

        Iterator<Integer> i = arrlist.iterator();
        i.remove();

    }
}
