package com.luxoft.dnepr.courses.regular.unit5;

import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.exception.WrongType;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import junit.framework.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/14/13
 * Time: 7:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class RedisTest {
    IDao<Redis> dao = new RedisDaoImpl();
    Redis employee1;
    Redis employee2;

    @Test
    public void testSave1() {
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        try {
            assertEquals(employee1, dao.save(employee1));
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testSave2() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
        Assert.assertEquals(1, EntityStorage.getEntities().size());
    }

    @Test
    public void testSave4() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
        try {
            assertEquals(dao.get(1l),EntityStorage.getEntities().get(1l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test(expected = UserAlreadyExist.class)
    public void testSave3() throws UserAlreadyExist {
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        employee2 = new Redis();
        employee2.setId(1l);
        employee2.setWeight(225);
        dao.save(employee1);
        dao.save(employee2);
    }

    @Test
    public void testUpdae1() throws UserAlreadyExist, UserNotFound {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        employee2 = new Redis();
        employee2.setId(1l);
        employee2.setWeight(225);
        dao.save(employee1);
        try {
            dao.update(employee2);
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

        Assert.assertEquals(EntityStorage.getEntities().get(1l),employee2);
    }

    @Test(expected = UserNotFound.class)
    public void testUpdae2() throws UserNotFound, UserAlreadyExist {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        employee2 = new Redis();
        employee2.setId(175l);
        employee2.setWeight(225);
        dao.save(employee1);
        try {
            dao.update(employee2);
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test(expected = UserNotFound.class)
    public void testUpdae3() throws UserAlreadyExist, UserNotFound {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        employee2 = new Redis();
       // employee2.setWeight();
        employee2.setWeight(225);
        dao.save(employee1);
        try {
            dao.update(employee2);
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test
    public void testGet1() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
        try {
            assertEquals(employee1,dao.get(1l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testGet2() {
        try {
            assertEquals(null,dao.get(100l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }

    }

    @Test
    public void testDelete1() {
        if(!EntityStorage.getEntities().isEmpty()) {
            EntityStorage.getEntities().clear();
        }
        employee1 = new Redis();
        employee1.setId(1l);
        employee1.setWeight(25);
        try {
            dao.save(employee1);
        } catch (UserAlreadyExist ex) {
            ex.printStackTrace();
        }
        try {
            assertEquals(true, dao.delete(1l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testDelete2() {
        try {
            assertEquals(false,dao.delete(100l));
        } catch (WrongType ex) {
            ex.printStackTrace();
        }
    }
}
