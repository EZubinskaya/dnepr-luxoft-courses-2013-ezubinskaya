package com.luxoft.dnepr.courses.compiler;

/**
 * Created with IntelliJ IDEA.
 * User: Cooper
 * Date: 27.10.13
 * Time: 10:54
 * To change this template use File | Settings | File Templates.
 */
public enum Operator {
    VOID("", 0, VirtualMachine.PRINT),
    PLUS("+", 1, VirtualMachine.ADD),
    MINUS("-", 2, VirtualMachine.SUB),
    MULTIPLY("*", 3, VirtualMachine.MUL),
    DIVISION("/", 4, VirtualMachine.DIV);

    public static final String BRACKET_OPEN = "(";
    public static final String BRACKET_CLOSE = ")";
    public static final int BRACKET_PRIORITY_BOOST = 10;

    private String operation;
    private int priority;
    private byte code;

    Operator(String operation, int  priority, byte code) {
        this.operation = operation;
        this.priority = priority;
        this.code = code;
    }
    public String getOperation() {
        return operation;
    }

    public int getPriority() {
        return priority;
    }

    public byte getCode() {
        return code;
    }

    public static boolean contains(String s) {
        for(Operator operator : values()) {
            if (operator.getOperation().equals(s)) {
                return true;
            }
        }
        return false;
    }

    public static Operator parseString(String input) {
        for (Operator operator : values())
        {
            if (operator.getOperation().equals(input)) {
                return operator;
            }
        }
        return Operator.VOID;
    }
}
