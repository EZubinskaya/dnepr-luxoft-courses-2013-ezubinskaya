package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Entity;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class AbstractDao<E extends Entity> implements IDao<E> {

    private JdbcTemplate jdbcTemplateObject;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplateObject) {
        this.jdbcTemplateObject=jdbcTemplateObject;
    }
}
