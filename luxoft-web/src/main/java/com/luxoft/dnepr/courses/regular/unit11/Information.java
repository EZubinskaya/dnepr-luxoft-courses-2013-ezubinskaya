package com.luxoft.dnepr.courses.regular.unit11;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/9/13
 * Time: 9:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class Information {
    private String name;
    private Integer age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }
}
