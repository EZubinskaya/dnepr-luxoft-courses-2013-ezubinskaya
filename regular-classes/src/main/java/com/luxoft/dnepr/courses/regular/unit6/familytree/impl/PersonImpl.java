package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.Gender;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonImpl implements Person {

    private String name;
    private String ethnicity;
    private Person father;
    private Person mother;
    private Gender gender;
    private int age;

    private final static String NULL = "null";

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getEthnicity() {
		return ethnicity;
	}
	
	@Override
	public Person getFather() {
		return father;
	}

	@Override
	public Person getMother() {
		return mother;
	}

	@Override
	public Gender getGender() {
		return gender;
	}

	@Override
	public int getAge() {
		return age;
	}


    public void setName(String name) {
        this.name = name;
    }


    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }


    public void setFather(Person father) {
        this.father = father;
    }


    public void setMother(Person mother) {
        this.mother = mother;
    }


    public void setGender(Gender gender) {
        this.gender = gender;
    }


    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonImpl)) return false;

        PersonImpl person = (PersonImpl) o;

        if (age != person.age) return false;
        if (ethnicity != null ? !ethnicity.equals(person.ethnicity) : person.ethnicity != null) return false;
        if (father != null ? !father.equals(person.father) : person.father != null) return false;
        if (gender != person.gender) return false;
        if (mother != null ? !mother.equals(person.mother) : person.mother != null) return false;
        if (name != null ? !name.equals(person.name) : person.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (ethnicity != null ? ethnicity.hashCode() : 0);
        result = 31 * result + (father != null ? father.hashCode() : 0);
        result = 31 * result + (mother != null ? mother.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + age;
        return result;
    }

    public String writePerson() {
        return writePerson(this);
    }

    public String writePerson(Person person) {
        if (person == null || !(person instanceof PersonImpl)) return PersonImpl.NULL;
        StringBuffer sb = new StringBuffer();
        sb.append("{")
                .append("\"name\":")
                .append(person.getName() == null ? PersonImpl.NULL: "\"" + escapeString(person.getName()) + "\"")
                .append(",\"ethnicity\":")
                .append(person.getEthnicity() == null ? PersonImpl.NULL: "\"" + escapeString(person.getEthnicity()) + "\"")
                .append(",\"gender\":")
                .append(person.getGender() == null ? PersonImpl.NULL: "\"" + escapeString(person.getGender().toString()) + "\"")
                .append(",\"age\":")
                .append("\"" + person.getAge() + "\"")
                .append(",\"father\":")
                .append(person.getFather() == null || !(person.getFather() instanceof PersonImpl) ? PersonImpl.NULL: writePerson(person.getFather()))
                .append(",\"mother\":")
                .append(person.getMother() == null  || !(person.getFather() instanceof PersonImpl) ? PersonImpl.NULL : writePerson(person.getMother()))
                .append("}");
        return sb.toString();
    }

    public static Person readPerson(String jsonString) throws PersonJSONParseException {
        PersonImpl result = new PersonImpl();
        if (PersonImpl.NULL.equals(jsonString)) return null;
        HashMap<String, String> parsedData = new HashMap<String, String>();
        if (jsonString.charAt(0) != '{' || jsonString.charAt(jsonString.length()-1) != '}') throw new PersonJSONParseException("Missed outer {/} for json object");
        jsonString = jsonString.substring(1, jsonString.length()-1);
        while(!jsonString.isEmpty()) {
            if (jsonString.indexOf("\"") != 0) throw new PersonJSONParseException("Missed opener \" for field name");
            int endNamePos = jsonString.indexOf("\"", 1);
            if (endNamePos <= 0) throw new PersonJSONParseException("Missed closer \" for field name");
            String name = jsonString.substring(1, endNamePos);
            String value;
            int endValuePos;
            if (jsonString.charAt(endNamePos+1) != ':') throw new PersonJSONParseException("Missed divider for field-value");
            if (PersonImpl.NULL.equals(jsonString.substring(endNamePos+2, endNamePos+6))) {
                value = null;
                endValuePos = endNamePos+7;
            } else {
                if (jsonString.charAt(endNamePos+2) == '{') {
                    //in case of }} or }, in field value we get oops :)
                    endValuePos = regexFind(jsonString, endNamePos+3, "(},)|(}$)");
                    if (endValuePos<0) throw new PersonJSONParseException("Missed closer } for field value");
                    value = jsonString.length() > endValuePos+2 ? jsonString.substring(endNamePos+2, endValuePos) : jsonString.substring(endNamePos+2);
                    endValuePos++;
                } else {
                    if (jsonString.charAt(endNamePos+2) != '"') throw new PersonJSONParseException("Missed opener \" for field value");
                    //in case of embeded " in field valie we get oops :)
                    endValuePos = jsonString.indexOf('"', endNamePos+3);
                    if (endValuePos<0) throw new PersonJSONParseException("Missed closer \" for field value");
                    value = jsonString.substring(endNamePos+3, endValuePos);
                    endValuePos += 2;
                }
            }
            endValuePos = Math.min(jsonString.length(), endValuePos);
            jsonString = jsonString.substring(endValuePos);
            parsedData.put(name, value);
        }
        for(Map.Entry<String, String> e : parsedData.entrySet()) {
            String value = e.getValue();
            switch (e.getKey()) {
                case "name":
                    result.setName(value);
                    break;
                case "ethnicity":
                    result.setEthnicity(e.getValue());
                    break;
                case "father":
                    result.setFather(value == null ? null : readPerson(value.toString()));
                    break;
                case "mother":
                    result.setMother(value == null ? null : readPerson(value.toString()));
                    break;
                case "gender":
                    result.setGender(Gender.valueOf(value));
                    break;
                case "age":
                    result.setAge(Integer.parseInt(value));
                    break;
                default:
                    break;
            }
        }
        return result;
    }

    private static int regexFind(String s, int startPos, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s.substring(startPos));
        return matcher.find() ? matcher.end() + startPos - 1 : -1;
    }

    public static class PersonJSONParseException extends Throwable {
        public PersonJSONParseException(String s) {
            super(s);
        }
    }

    protected static String escapeString(String s) {
        if(s==null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<s.length();i++) {
            char ch=s.charAt(i);
            switch(ch) {
                case '"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                default:
                    if((ch>='\u0000' && ch<='\u001F') || (ch>='\u007F' && ch<='\u009F') || (ch>='\u2000' && ch<='\u20FF')) {
                        String ss=Integer.toHexString(ch);
                        sb.append("\\u");
                        for(int k=0;k<4-ss.length();k++) {
                            sb.append('0');
                        }
                        sb.append(ss.toUpperCase());
                    } else {
                        sb.append(ch);
                    }
            }
        }
        return sb.toString();
    }
}
