package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.util.List;
import  java.util.concurrent.*;

/**
 * Crawls files and directories, searches for text files and counts word occurrences.
 */
public class FileCrawler {

    private final static int PARSER_QUEUE_SIZE = 1024;
    public LinkedBlockingQueue<String> parserQueue = new LinkedBlockingQueue<String>(PARSER_QUEUE_SIZE);

    protected String rootFolder;
    protected int maxNumberOfThreads;

    private WordStorage wordStorage = new WordStorage();



    public FileCrawler(String rootFolder, int maxNumberOfThreads) {
        this.rootFolder = rootFolder;
        this.maxNumberOfThreads = maxNumberOfThreads;
    }

    /**
     * Performs crawling using multiple threads.
     * This method should wait until all parallel tasks are finished.
     * @return FileCrawlerResults
     */
    public FileCrawlerResults execute() {
        ExecutorService executor = Executors.newFixedThreadPool(maxNumberOfThreads);
        CountDownLatch parserLatch = new CountDownLatch(maxNumberOfThreads - 1);
        for (int i=1; i<maxNumberOfThreads; i++) {
            executor.execute(new ParserThread(parserQueue, parserLatch, wordStorage));
        }
        try {
            List<File> futureFileSystem = executor.submit(new FileSystemThread(parserQueue, rootFolder)).get();
            executor.shutdown();
            parserLatch.await();
            return new FileCrawlerResults(futureFileSystem, wordStorage.getWordStatistics());
        } catch (InterruptedException | ExecutionException e) {
            executor.shutdownNow();
        }
        return null;
    }

    public static void main(String args[]) {
        String rootDir = args.length > 0 ? args[0] : System.getProperty("user.home");
        FileCrawler crawler = new FileCrawler(rootDir, 10);
        crawler.execute();
    }

}
