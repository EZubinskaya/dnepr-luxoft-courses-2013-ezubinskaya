package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/6/13
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class DummyServlet extends HttpServlet {

    ConcurrentHashMap<String,Integer> data = new ConcurrentHashMap<String,Integer>();

    private synchronized Information readData(HttpServletRequest request) throws  IOException {

        Information helpRead = new Information();
        String name = null;
        Integer age = null;
        String line = null;
        StringBuilder jb = new StringBuilder();
        BufferedReader reader = request.getReader();
        while ((line = reader.readLine()) != null) {
            jb.append(line);
        }
        JSONObject jsonObject = new JSONObject(jb.toString());
        try {
            name = jsonObject.getString("name");
            age = jsonObject.getInt("age");
        } catch(JSONException | NumberFormatException e) {
            return null;
        }
        helpRead.setName(name);
        helpRead.setAge(age);
        return helpRead;
    }

    private Map<HttpServletResponse, Boolean> helpResponse(Information helpRead, HttpServletResponse response) throws IOException {
        Map<HttpServletResponse,Boolean> help = new HashMap<>();
        if (helpRead == null ) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("{\"error\": \"Illegal parameters\"}");
            help.put(response,true);
        }
        else if (data.containsKey(helpRead.getName())) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            String answer = "{\"error\": \"Name "+helpRead.getName()+" already exists\"}";
            response.getWriter().write(answer);
            help.put(response,true);
        } else {
            help.put(response,false);
        }
        return help;
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Information helpRead = readData(request);

        Map<HttpServletResponse,Boolean> help= helpResponse(helpRead,response);
        if (help.get(response) == false ) {
            data.put(helpRead.getName(), helpRead.getAge());
            response.setStatus(HttpServletResponse.SC_CREATED);
        }
    }

    public void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Information helpRead = readData(request);
        Map<HttpServletResponse,Boolean> help= helpResponse(helpRead,response);
        if (help.get(response) == false) {
            data.put(helpRead.getName(), helpRead.getAge());
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        }
    }
}
