package com.luxoft.dnepr.courses.unit1;

import java.nio.charset.StandardCharsets;

public final class LuxoftUtils {

    // Third task
    public static String binaryTodecimal(String binaryNumber) {
        byte[] bytes = binaryNumber.getBytes(StandardCharsets.US_ASCII);
        byte[] binaryPossible = { '0', '1' };
        if (check(bytes, binaryPossible) == false) {
            return "Not binary";
        }
        int radix = 2;
        int exp = 0;
        int result = 0;
        for (int i = bytes.length - 1; i >= 0; i--) {
            result += (int) Math.pow((double) radix, (double) exp++)
                    * getIntValue(bytes[i]);
        }
        return Integer.toString(result);
    }

    private static int getIntValue(byte character) {
        if (character >= '0' && character <= '9') {
            return character - '0';
        }
        return -1;
    }

    private static boolean check(byte[] bytes, byte[] binaryPossible) {
        for (int i = 0; i < bytes.length; i++) {
            if (bytes[i] != binaryPossible[0] && bytes[i] != binaryPossible[1]) {
                //bytes[i] = 32, if symbol is " "
                if(i ==  bytes.length-1 && bytes[i]==32)   {} else {
                    return false;   }
            }
        }
        return true;
    }

    // Four task
    protected static String decimalToBinary(String decimalNumber) {
        byte[] bytes = decimalNumber.getBytes(StandardCharsets.US_ASCII);
        for (int i = 0; i < bytes.length; i++) {
            if (getIntValue(bytes[i]) == -1) {
                return "Not decimal";
            }
        }
        int radix = 2;
        String result = "";
        int decimalNumb = Integer.parseInt(decimalNumber);
        while (decimalNumb > 0) {
            result = String.valueOf(decimalNumb % radix) + result;
            decimalNumb /= radix;
        }
        return result;
    }

    // Five task
    protected static int[] sortArray(int[] array, boolean asc) {
        int[] helpArray = array.clone();
        if (asc) {
            for (int j = 1; j < array.length; j++) {
                for (int i = 0; i < array.length - j; i++) {
                    if (helpArray[i] > helpArray[i + 1]) {
                        int c = helpArray[i];
                        helpArray[i] = helpArray[i + 1];
                        helpArray[i + 1] = c;
                    }
                }
            }

        } else {
            for (int j = 1; j < array.length; j++) {
                for (int i = 0; i < array.length - j; i++) {
                    if (helpArray[i] < helpArray[i + 1]) {
                        int c = helpArray[i];
                        helpArray[i] = helpArray[i + 1];
                        helpArray[i + 1] = c;
                    }
                }
            }
        }
        return helpArray;
    }

    // Second task
    protected static String getMonthName(int monthOrder, String language) {
        String name = "";
        if (checks(language, monthOrder) != "") {
            return checks(language, monthOrder);
        }

        switch (monthOrder) {
            case 1:
                switch (language) {
                    case "en":
                        return "January";

                    case "ru":
                        return "Январь";

                }
                break;
            case 2:
                switch (language) {
                    case "en":
                        return "Fabruary";
                    case "ru":
                        return "Февраль";
                }
                break;

            case 3:
                switch (language) {
                    case "en":
                        return "March";
                    case "ru":
                        return "Март";
                }
                break;

            case 4:
                switch (language) {
                    case "en":
                        return "April";

                    case "ru":
                        return "Апрель";
                }
                break;

            case 5:
                switch (language) {
                    case "en":
                        return "May";

                    case "ru":
                        return "Май";
                }
                break;

            case 6:
                switch (language) {
                    case "en":
                        return "June";

                    case "ru":
                        return "Июнь";
                }
                break;

            case 7:
                switch (language) {
                    case "en":
                        return "July";
                    case "ru":
                        return "Июль";
                }
                break;

            case 8:
                switch (language) {
                    case "en":
                        return "August";

                    case "ru":
                        return "Август";
                }
                break;

            case 9:
                switch (language) {
                    case "en":
                        return "September";

                    case "ru":
                        return "Сентябрь";

                }
                break;

            case 10:
                switch (language) {
                    case "en":
                        return "October";

                    case "ru":
                        return "Октябрь";

                }
                break;

            case 11:
                switch (language) {
                    case "en":
                        return "November";

                    case "ru":
                        return "Ноябрь";
                }
                break;
            case 12:
                switch (language) {
                    case "en":
                        return "December";
                    case "ru":
                        return "Декабрь";
                }
                break;
        }
        return name;
    }

    private static String checks(String language, int monthOrder) {
        boolean check = true;
        if (language != "en" && language != "ru") {
            return "Unknown Language";

        }
        if (monthOrder < 1 || monthOrder > 12) {
            switch (language) {
                case "en":
                    return "Unknown Month";
                case "ru":
                    return "Неизвестный месяц";
            }


        }
        return "";
    }


}