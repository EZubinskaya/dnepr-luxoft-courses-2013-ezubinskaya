package com.luxoft.dnepr.courses.regular.unit6.familytree_version1.io;

import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.JSONSerialize;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Scanner;

public class IOUtils {
	
	private IOUtils() {
	}
	
	public static FamilyTree load(String filename) throws FileNotFoundException, JSONSerialize.WrongJSONException, JSONSerialize.UnMapException {
        InputStream is = new FileInputStream(filename);
        return load(is);
	}
	
	public static FamilyTree load(InputStream is) throws JSONSerialize.WrongJSONException, JSONSerialize.UnMapException {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return (FamilyTree) JSONSerialize.unserialize(s.hasNext() ? s.next() : "");
	}
	
	public static void save(String filename, FamilyTree familyTree) throws IOException {
        OutputStream os = new FileOutputStream(filename, false);
        save(os, familyTree);
	}
	
	public static void save(OutputStream os, FamilyTree familyTree) throws IOException {
        os.write(JSONSerialize.serialize(familyTree).getBytes(Charset.forName("UTF-8")));
    }
}
