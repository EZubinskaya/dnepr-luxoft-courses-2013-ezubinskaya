package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.model.Employee;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/13/13
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmployeeDaoImpl extends AbstractDAO<Employee> {

    public EmployeeDaoImpl() {
        super(Employee.class);
    }
}
