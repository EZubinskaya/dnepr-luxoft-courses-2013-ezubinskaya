package com.luxoft.dnepr.courses.compiler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Cooper
 * Date: 27.10.13
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
public class ExpressionMatcher {

    private Matcher matcher;
    private final String expressionPattern = "(((\\d(\\.\\d)?)+)|[+\\-\\*/()])";
    private String founded = null;
    private int currentPrioity = 0;

    private int bracket =0;

    ExpressionMatcher(String input) {
        Pattern p = Pattern.compile(expressionPattern);
        matcher = p.matcher(input);
    }

    boolean find() {
       if (matcher.find()) {
           founded = matcher.group();
           switch (founded)
           {
               case Operator.BRACKET_OPEN:
                   currentPrioity += Operator.BRACKET_PRIORITY_BOOST;
                   bracket = getBracket() +1;
                   setBracket(bracket);
                   return find();
               case Operator.BRACKET_CLOSE:
                   currentPrioity -= Operator.BRACKET_PRIORITY_BOOST;
                   bracket = getBracket() -1;
                   setBracket(bracket);
                   return find();
               default:
                   if (Operator.contains(founded)) {
                       Operator currentOp = Operator.parseString(founded);
                       boolean firstDivMul = (currentOp == Operator.DIVISION || currentOp == Operator.MULTIPLY);
                       int minusTotal = (currentOp == Operator.MINUS) ? 1 : 0;
                       int searchPos = matcher.end();
                       while (matcher.find() && (Operator.contains(matcher.group())))
                       {
                           searchPos = matcher.end();
                           founded = matcher.group();
                           currentOp = Operator.parseString(founded);
                           if (firstDivMul || (currentOp == Operator.DIVISION || currentOp == Operator.MULTIPLY)) {
                               throw new CompilationException("Wrong operator's syntax");
                           }
                           minusTotal += (currentOp == Operator.MINUS) ? 1 : 0;
                       }
                       if (matcher.hitEnd() || !Operator.contains(matcher.group())) {
                           matcher.region(searchPos, matcher.regionEnd());
                       }
                       if (minusTotal > 0) {
                           founded = (minusTotal % 2 == 1) ? Operator.MINUS.getOperation() : Operator.PLUS.getOperation();
                       }
                   }

                   return true;
           }
       } else {
           return false;
       }
    }

    String getFounded() {
        return founded;
    }

    int getCurrentPrioity() {
        return currentPrioity;
    }
    void setBracket(int b) {
        bracket=b;
    }
    int getBracket() {
        return bracket;
    }
}
