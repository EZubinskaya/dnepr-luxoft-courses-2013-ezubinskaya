package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/13/13
 * Time: 7:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private final static Map<Long, Entity> entities = new HashMap();
    private EntityStorage() {}
    public static Map<Long, Entity> getEntities() {
        return entities;
    }
}