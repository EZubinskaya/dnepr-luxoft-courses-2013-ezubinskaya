/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/1/13
 * Time: 1:49 PM
 * To change this template use File | Settings | File Templates.
 */
$(function() {
    $("#menu button").button();

    function serviceRequest(serviceName, callback) {
        serviceName &&
            $.ajax({
                url: '/service?method=' + serviceName,
                dataType : 'text'
            }).done(function(response){
                var response = jQuery.parseJSON(response);
                if (response) {
                    callback(response);
                } else {
                    alert("We all die !!!");
                }
            })

    }

    function menuPrepareNewGame() {
        $('#new-game-btn').hide();
        $('#menu').css('top', '115px');
        $('#hit-btn').show();
        $('#stand-btn').show();
    }

    function drawHand(hand, cards) {
        hand.empty();
        if (cards && cards.length > 0) {
            for(var i in cards) {
                cards[i].suit && cards[i].rank && hand.append(
                    '<img src="/img/' + cards[i].suit + '_'+cards[i].rank + '.png"/>'
                );
            }
        }
    }

    function printMsg(msg) {
        $('#result-popup p').text(msg);
        $('#result-popup').dialog({
            modal: true,
            buttons: [{
                text: "Thanks, it's very cool game !!!",
                click: function() {
                    window.location.href = "/";
                }
            }]
        });
    }

    $('#new-game-btn').click(function(){
        serviceRequest('startGame', function(response){
            menuPrepareNewGame();
            drawHand($('#my-hand'), response.myHand);
            drawHand($('#dealers-hand'), response.dealersHand);
        })
    });

    $('#hit-btn').click(function(){
        serviceRequest('requestMore', function(response){
            drawHand($('#my-hand'), response.myHand);
            if (!response.result) {
                printMsg('You lose !');
            }
        });
    });

    $('#stand-btn').click(function(){
        serviceRequest('requestStop', function(response){
            drawHand($('#dealers-hand'), response.dealersHand);
            switch (response.result) {
                case 'WIN':
                    printMsg('You win !');
                    break;
                case 'LOOSE':
                    printMsg('You lose !');
                    break;
                case 'PUSH':
                    printMsg('It\'s a draw !');
                    break;
                default:
                    printMsg('We all die !!!');
                    break;
            }
        });
    });
});
