package com.luxoft.dnepr.courses.regular.unit3;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/6/13
 * Time: 9:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class User implements UserInterface {

    private Long id;
    private String name;
    private WalletInterface wallet;
    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return wallet;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        wallet.setOwner(this);
        this.wallet = wallet;
    }
}
