package com.luxoft.dnepr.courses.regular.unit11;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/6/13
 * Time: 2:21 PM
 */
public class GetHitCountServlet extends HttpServlet {

    private AtomicInteger hitCount = new AtomicInteger(0);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        hitCount.set(hitCount.get()+1);

        response.getWriter().write("{\"hitCount\": "+hitCount+"}");
    }
}
