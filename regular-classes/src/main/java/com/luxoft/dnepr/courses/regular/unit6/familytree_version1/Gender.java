package com.luxoft.dnepr.courses.regular.unit6.familytree_version1;

public enum Gender {
	
	MALE, FEMALE;
}
