package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.exception.WrongType;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collection;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/13/13
 * Time: 7:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class AbstractDAO<E extends Entity> implements IDao<E> {

    private Class<E> type;
    public AbstractDAO(Class<E> type) {
        this.type = type;

    }
    public E create() {
        try{
            return type.newInstance();
        } catch(Exception e) {
            return null;
        }
    }

    @Override
    public E save(E e) throws UserAlreadyExist {

        Long id = e.getId();
        if(findSame(e) == null) {

            if(id == null) {
                id = findMax(e)+1;
                e.setId(id);
            }
            EntityStorage.getEntities().put(id, e);
        } else {
            throw new UserAlreadyExist();
        }
        return e;
    }

    private Long findMax(E ex) {

        Long max = 0l;
        Long currentId = ex.getId();
        Map<Long, Entity> datas = EntityStorage.getEntities();

        for (Map.Entry<Long, Entity> entry : datas.entrySet()) {
            Long id = entry.getKey();
            if(max < id) {
                max = id;
            }
        }
        return max;
    }

    private Long findSame(E e) {
        Map<Long, Entity> datas = EntityStorage.getEntities();
        for (Map.Entry<Long, Entity> entry : datas.entrySet()) {
            if(entry.getKey().equals(e.getId())){
                return e.getId();
            }
        }
        return null;
    }

    @Override
    public E update(E e) throws UserNotFound, WrongType {

        Long id = e.getId();
        if (id == null || (id = findSame(e)) == null){
            throw new UserNotFound();
        }

        Map<Long, Entity> datas = EntityStorage.getEntities();
        for(Map.Entry<Long, Entity> entry : datas.entrySet()) {

            if(entry.getKey() == id) {

                if (!(e.getClass().isAssignableFrom(entry.getValue().getClass()))) {
                    throw new WrongType();
                }
                datas.put(id,e);
                return e;
            }
        }
        return null;
    }

    @Override
    public E get(long id) throws WrongType {

        Map<Long, Entity> entities = EntityStorage.getEntities();
        for (Map.Entry<Long, Entity> entry : entities.entrySet()) {

            if(entry.getKey().equals(id)){
                Entity value = entry.getValue();

                if (!value.getClass().isAssignableFrom(type)) {
                    throw new WrongType();
                }
                return (E) value;
            }
        }
        return null;
    }

    @Override
    public boolean delete(long id) throws WrongType {

        Map<Long, Entity> entities = EntityStorage.getEntities();
        for (Map.Entry<Long, Entity> entry : entities.entrySet()) {

            if(entry.getKey().equals(id)){
                Entity value = entry.getValue();

                if (!value.getClass().isAssignableFrom(type)) {
                    throw new WrongType();
                }
                entities.remove(entry.getKey());
                return true;
            }
        }
        return false;
    }

}
