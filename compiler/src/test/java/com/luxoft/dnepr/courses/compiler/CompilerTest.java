package com.luxoft.dnepr.courses.compiler;



import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CompilerTest extends AbstractCompilerTest {

	@Test
	public void testSimple() {
		assertCompiled(4, "2+2");
		assertCompiled(5, " 2 + 3 ");
		assertCompiled(1, "2 - 1 ");
		assertCompiled(6, " 2 * 3 ");
		assertCompiled(4.5, "  9 /2 ");
	}
	
	@Test
	public void testComplex() {
		assertCompiled(12, "  (2 + 2 ) * 3 ");
		assertCompiled(8.5, "  2.5 + 2 * 3 ");
		assertCompiled(8.5, "  2 *3 + 2.5");
        assertCompiled(1, "-(-(+(-(+5))))+6");
        assertCompiled(16, "(3 + 5) * 2");
        assertCompiled(0, "( 3 +  6 ) - 9");
        assertCompiled(5.99, "2.55 + 3.44");
	}

    @Test(expected = CompilationException.class)
    public void testComplex2() {
        Compiler c = new Compiler();
        c.compile("-(-(+(-(+5)+6");
   }

    @Test(expected = CompilationException.class)
    public void testComplex3() {
        Compiler c = new Compiler();
        c.compile("");
    }


}
