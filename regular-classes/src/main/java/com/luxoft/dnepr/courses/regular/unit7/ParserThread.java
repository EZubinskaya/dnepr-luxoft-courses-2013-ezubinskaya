package com.luxoft.dnepr.courses.regular.unit7;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/22/13
 * Time: 11:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParserThread implements Runnable {

    protected LinkedBlockingQueue<String> queue;
    protected CountDownLatch latch;
    protected WordStorage wordStorage;

    public ParserThread(LinkedBlockingQueue<String> queue, CountDownLatch latch, WordStorage wordStorage) {
        this.queue = queue;
        this.latch = latch;
        this.wordStorage = wordStorage;
    }

    public void run() {
        try {
            while (true) {
                String fileName = queue.take();
                if (fileName == FileSystemThread.FILE_LIST_END) {
                    queue.put(FileSystemThread.FILE_LIST_END);
                    break;
                }
                try {
                    parseFile(fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        latch.countDown();
    }

    public void parseFile(String fileName) throws IOException {
        Scanner scanner = new Scanner(new File(fileName));
        scanner.useDelimiter("[-–.,:=_;()?!\" \t\n\r]+");
        while (scanner.hasNext()) {
            String line = scanner.next();
            String word = line.replaceAll("[^\\p{L}\\p{Nd}]", "");
            wordStorage.save(word);
        }

    }
}
