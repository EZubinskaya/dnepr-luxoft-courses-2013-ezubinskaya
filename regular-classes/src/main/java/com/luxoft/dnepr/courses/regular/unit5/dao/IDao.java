package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.exception.WrongType;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/13/13
 * Time: 7:14 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IDao<E extends Entity> {
    E save (E e) throws UserAlreadyExist;
    E update (E e) throws UserNotFound, WrongType;
    E get (long id) throws WrongType;
    boolean delete (long id) throws WrongType;
}