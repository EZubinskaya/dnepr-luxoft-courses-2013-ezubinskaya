package com.luxoft.dnepr.courses.regular.unit12;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/11/13
 * Time: 4:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class AppContextListener implements ServletContextListener {

    public synchronized void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext ctx = servletContextEvent.getServletContext();
        HashMap<String,String> data = new HashMap<>();
        try
        {
            String path = ctx.getInitParameter("users");
            InputStream in = ctx.getResourceAsStream("/META-INF/" + path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(in);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("user");

            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    data.put(eElement.getAttribute("name"), eElement.getAttribute("password"));
                }
            }

            ctx.setAttribute("Database", data);

        } catch (Exception  e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}