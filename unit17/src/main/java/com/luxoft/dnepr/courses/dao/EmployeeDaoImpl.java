package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Employee;
import com.luxoft.dnepr.courses.model.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

public class EmployeeDaoImpl extends AbstractDao<Employee> {

    @Autowired
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public Employee save(Employee employee) {
        String SQL = "insert into employee (id, salary) values (?, ?)";
        jdbcTemplateObject.update( SQL, employee.getId(), employee.getSalary());
        return employee;
    }

    @Override
    public Employee update(Employee employee) {
        String SQL = "update employee set salary = ? where id = ?";
        jdbcTemplateObject.update( SQL, employee.getSalary(),employee.getId());
        return employee;
    }

    @Override
    public Employee get(long id) {
        String SQL = "select * from employee where id = ?";
        Employee employee = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new EmployeeMapper());
        return employee;
    }

    @Override
    public boolean delete(long id) {
        String SQL = "delete from employee where id = ?";
        jdbcTemplateObject.update(SQL, id);
        return true;
    }

    @Override
    public List<Employee> getAll() {
        String SQL = "select * from employee";
        List <Employee> employees = jdbcTemplateObject.query(SQL,
                new EmployeeMapper());
        return employees;
    }


}
