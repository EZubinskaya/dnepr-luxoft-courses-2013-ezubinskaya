package com.luxoft.dnepr.courses.unit3.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WebServer {
	
	private final int port;
	private volatile ExecutorService pool;
	
	public WebServer(int port) {
		this.port = port;
	}

	public void start() throws IOException {
		pool = Executors.newFixedThreadPool(16);

		ServerSocket socket = new ServerSocket(port);
		
		while (!Thread.interrupted()) {
			final Socket client = socket.accept();
			pool.submit(new Runnable() {

				@Override
				public void run() {
					handleRequest(client);
				}
				
			});
		}
		pool.shutdownNow();
	}

	private void handleRequest(Socket client) {
		try {
			processRequest(client);
		} catch (Throwable th) {
			try {
				th.printStackTrace();
				writeStatus500(client, th);
			} catch (Throwable ex) {// just print:
				ex.printStackTrace();
			}
		} finally {
			try {
				client.close();
			} catch (IOException ex) {
			}
		}
	}

	private void processRequest(Socket client) throws IOException {
		Request request = createRequest(client);
		if (request == null) {
			//writeStatus400(client);
			return;
		}
		Response response = dispatch(request);
		if (response == null) {
			writeStatus404(client);
		} else {
			writeStatus200(client, response);
		}
	}
	
	@SuppressWarnings("unused")
	private void writeStatus400(Socket client) throws IOException {
		writeStatus(client, 400, "Bad request", "text/html", "<h3>Status 400: Bad request</h3>".getBytes());
	}

	private void writeStatus404(Socket client) throws IOException {
		writeStatus(client, 404, "Not found",  "text/html", "<h3>Status 404: Not found</h3>".getBytes());
	}

	private void writeStatus200(Socket client, Response response) throws IOException {
		writeStatus(client, 200, response.getContentType(), "OK", response.getBody());
	}

	private void writeStatus500(Socket client, Throwable th) throws IOException {
		StringWriter result = new StringWriter();
		PrintWriter body = new PrintWriter(result);
		body.print("<h3>Status 500: Server error</h3><pre>");
		th.printStackTrace(body);
		body.print("</pre>");
		body.flush();
	
		writeStatus(client, 500, "Server error", "text/html", result.toString().getBytes());
	}
	
	private void writeStatus(Socket client, int status, String contentType, String message, byte[] content) throws IOException {
		OutputStream result = client.getOutputStream();
		String header = "HTTP/1.1 " + status + " " + message + "\n" +
				"Content-Type: " + contentType + "\n\n";
		result.write(header.getBytes());
		result.write(content);
		result.flush();
		result.close();
	}

	private Request createRequest(Socket client) throws IOException {
		Reader inputMessage = new InputStreamReader(client.getInputStream());
		String header = readHeader(inputMessage);
		String[] parts = header.split(" ");
		if (parts.length != 3) {
			return null;
		}
		String method = parts[0];
		String uri = parts[1];
		skipToBody(inputMessage);
		
		return new Request(method, uri, inputMessage);
	}

	private String readHeader(Reader inputMessage) throws IOException {
		StringBuilder result = new StringBuilder();
		int next;
		while ((next = inputMessage.read()) >= 0) {
			if (next == '\n' || next == '\r') {
				return result.toString();
			}
			result.appendCodePoint(next);
		}
		return result.toString();
	}

	private void skipToBody(Reader inputMessage) throws IOException {
		int next;
		while (inputMessage.ready() && (next = inputMessage.read()) >= 0) {
			if (next != '\n' && next != '\r') {
				continue;
			}
			if (!inputMessage.ready()) {
				return;
			}
			int following = inputMessage.read();
			if (next == '\r' && following == '\n') {
				if (!inputMessage.ready()) {
					return;
				}
				following = inputMessage.read();
			}
			if (following < 0 || following == '\n' || following == '\r') {
				return;
			}
			
		}
	}

	private Response dispatch(Request request) throws IOException {
		String uri = request.getUri();
		if (uri.equals("/")) {
			uri = "/index.html";
		}
		InputStream resource = getClass().getResourceAsStream("/com/luxoft/dnepr/courses/unit3/web" + uri);
		if (resource != null) {
			return dispatchToResource(resource, uri);
		}
		return dispatchToMethod(request);
	}

	private Response dispatchToResource(InputStream resource, String uri)
			throws IOException {
		try {
			return new Response(resource, getContentType(uri));
		} finally {
			resource.close();
		}
	}

	private String getContentType(String uri) {
		if (uri.endsWith(".htm") || uri.endsWith(".html")) {
			return "text/html";
		}
		if (uri.endsWith(".css")) {
			return "text/css";
		}
		if (uri.endsWith(".js")) {
			return "text/javascript";
		}
		return "text/plain";
	}

	private Response dispatchToMethod(Request request) {
		if (!request.getUri().equals("/service")) {
			return null;
		}
		Response response = new Response();
		return new MethodDispatcher().dispatch(request, response);
	}
	
	
}
