SELECT AVG(pc.hd) as avg_hd FROM makers INNER JOIN product ON (makers.maker_id = product.maker_id) INNER JOIN pc ON (product.model = pc.model)
WHERE makers.maker_id IN (SELECT DISTINCT(makers.maker_id) FROM makers INNER JOIN product ON (makers.maker_id = product.maker_id) WHERE type = "Printer")
