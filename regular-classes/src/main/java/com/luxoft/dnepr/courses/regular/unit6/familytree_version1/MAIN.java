package com.luxoft.dnepr.courses.regular.unit6.familytree_version1;

import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.impl.FamilyTreeImpl;
import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.impl.PersonImpl;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/17/13
 * Time: 9:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class MAIN {
    public static void main(String[] args) {
        PersonImpl p = new PersonImpl();
        p.setAge(23);
        p.setName("Mark\b");
        p.setEthnicity("en");
        p.setFather(null);
        p.setMother(null);
        p.setGender(Gender.MALE);
        PersonImpl sara = new PersonImpl();
        sara.setAge(23);
        sara.setName("Sara");
        sara.setEthnicity("en");
        sara.setFather(null);
        sara.setMother(null);
        sara.setGender(Gender.FEMALE);
        PersonImpl finishPerson = new PersonImpl();
        finishPerson.setAge(10);
        finishPerson.setName("Lola");
        finishPerson.setEthnicity("en");
        finishPerson.setFather(p);
        finishPerson.setMother(sara);
        finishPerson.setGender(Gender.FEMALE);


        FamilyTree ft = FamilyTreeImpl.create(finishPerson);


        String json = JSONSerialize.serialize(ft);
        System.out.println(json);
        FamilyTree tf = null;
        try {
            tf = (FamilyTree) JSONSerialize.unserialize(json);
        } catch (JSONSerialize.WrongJSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (JSONSerialize.UnMapException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        System.out.println(tf.toString());
    }
}
