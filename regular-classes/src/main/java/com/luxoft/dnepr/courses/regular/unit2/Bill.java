package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private List<CompositeProduct> compositeProducts = new ArrayList<CompositeProduct>();

    /**
     * Appends new instance of product into the bill. Groups all equal products
     * using {@link CompositeProduct}
     *
     * @param product
     *            new product
     */
    public void append(Product product) {
        for (CompositeProduct cp : compositeProducts) {
            if (cp.contains(product)) {
                cp.add(product);
                return;
            }
        }
        CompositeProduct newCp = new CompositeProduct(product);
        compositeProducts.add(newCp);
    }

    /**
     * Calculates total cost of all the products in the bill including
     * discounts.
     *
     * @return
     */
    public double summarize() {
        double summarize = 0d;
        for (CompositeProduct cp : compositeProducts) {
            summarize += cp.getPrice();
        }
        return summarize;
    }

    /**
     * Returns ordered list of products, all equal products are represented by
     * single element in this list. Elements should be sorted by their price in
     * descending order. See {@link CompositeProduct}
     *
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<Product> getProducts() {
        Collections.sort(compositeProducts, new CompositeProductComparator());
        return (List<Product>) (ArrayList) compositeProducts;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

    class CompositeProductComparator implements Comparator<CompositeProduct> {
        @Override
        public int compare(CompositeProduct o1, CompositeProduct o2) {
            return o1.getPrice() > o2.getPrice() ? -1 : (o1.getPrice() < o2
                    .getPrice() ? 1 : 0);
        }
    }
}
