package com.luxoft.dnepr.courses.unit3.web;

import java.io.IOException;
import java.util.List;

import com.luxoft.dnepr.courses.unit3.controller.Deck;
import com.luxoft.dnepr.courses.unit3.controller.GameController;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class MethodDispatcher {

	/**
	 * 
	 * @param request
	 * @param response
	 * @return response or <code>null</code> if wasn't able to find a method.
	 */
	public Response dispatch(Request request, Response response) {
		String method = request.getParameters().get("method");
        switch (method) {
            case "requestMore":
                return requestMore(response);
            case "requestStop":
                return requestStop(response);
            case "startGame":
                return startGame(response);
            default:
                return null;
        }
	}

	private Response requestMore(Response response) {
		response.write("{\"result\":" + GameController.getInstance().requestMore());
		response.write(",\"myHand\":");
		writeHand(response, GameController.getInstance().getMyHand());
		response.write("}");
		return response;
	}

    private Response requestStop(Response response) {
        GameController.getInstance().requestStop();
        response.write("{\"result\":\"" + GameController.getInstance().getWinState().toString());
        response.write("\",\"dealersHand\":");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");
        return response;
    }

    private Response startGame(Response response) {
        GameController controller = GameController.getInstance();
        controller.newGame();
        response.write("{\"myHand\":");
        writeHand(response, GameController.getInstance().getMyHand());
        response.write(",\"dealersHand\":");
        writeHand(response, GameController.getInstance().getDealersHand());
        response.write("}");
        return response;
    }

	private void writeHand(Response response, List<Card> hand) {
		boolean isFirst = true;
		response.write("[");
		for (Card card : hand) {
			if (isFirst) {
				isFirst = false;
			} else {
				response.write(",");
			}
			response.write("{\"rank\":\"");
			response.write(card.getRank().getName());
			response.write("\",\"suit\":\"");
			response.write(card.getSuit().name());
			response.write("\",\"cost\":");
			response.write(String.valueOf(card.getCost()));
			response.write("}");
		}
		response.write("]");
	}

}
