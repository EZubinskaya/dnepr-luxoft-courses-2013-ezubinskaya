package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {

    private List<Product> childProducts = new ArrayList<Product>();

    private double totalPrice = 0d;

    CompositeProduct() {
    }

    CompositeProduct(Product product) {
        add(product);
    }

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (childProducts.isEmpty()) {
            return null;
        } else {
            return childProducts.get(0).getCode();
        }
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (childProducts.isEmpty()) {
            return null;
        } else {
            return childProducts.get(0).getName();
        }
    }

    /**
     * Returns total price of all the child products taking into account
     * discount. 1 item - no discount 2 items - 5% discount >= 3 items - 10%
     * discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        return Discount.getPrice(totalPrice, getAmount());
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        totalPrice += product.getPrice();
        childProducts.add(product);
    }

    public void remove(Product product) {
        totalPrice -= product.getPrice();
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }

    public boolean contains(Product product) {
        return childProducts.isEmpty() ? false : ((AbstractProduct) product).equals(childProducts.get(0));
    }

}
