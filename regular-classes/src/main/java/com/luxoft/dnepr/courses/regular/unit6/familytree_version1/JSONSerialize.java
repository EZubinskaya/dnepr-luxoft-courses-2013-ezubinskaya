package com.luxoft.dnepr.courses.regular.unit6.familytree_version1;

import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/17/13
 * Time: 9:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class JSONSerialize {

    public static class WrongJSONException extends Exception {
        public WrongJSONException(String message) {
            super(message);
        }
    }

    public static class UnMapException extends Exception {
        public UnMapException(String message) {
            super(message);
        }
    }

    protected static ObjectField getClassField(Object obj, String name) {
        ObjectField data = new ObjectField();
        data.name = name;
        Class c = obj.getClass();
        try {
            Field field = c.getDeclaredField(name);
            field.setAccessible(true);
            data.type = field.getType().getName().toString();
            data.value = field.get(obj);
            data.isSimple = field.getType().isPrimitive() || String.class.isAssignableFrom(field.getType()) ||
                    field.getType().isEnum() || data.value == null;
            data.isAllowed = !(java.lang.reflect.Modifier.isStatic(field.getModifiers()) ||
                    java.lang.reflect.Modifier.isTransient(field.getModifiers()));
        } catch (IllegalAccessException | NoSuchFieldException e) {
        }
        return data;
    }

    protected static ObjectEntity map(Object obj) {
        ObjectEntity result = new ObjectEntity();
        ObjectEntity oe;
        Class c = obj.getClass();
        ObjectField of = getClassField(obj, "serialVersionUID");
        result.className = c.getName();
        result.version = of.value == null ? null : (Long) of.value;
        Field[] fields = c.getDeclaredFields();
        for(int i = 0; i<fields.length; i++){
            of = getClassField(obj, fields[i].getName());
            if (of.isAllowed) {
                if (of.isSimple) {
                    result.fields.add(of);
                } else {
                    oe = map(of.value);
                    oe.name = of.name;
                    result.entities.add(oe);
                }
            }
        }
        return result;
    }

    protected static String escapeString(String s) {
        if(s==null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for(int i=0;i<s.length();i++) {
            char ch=s.charAt(i);
            switch(ch) {
                case '"':
                    sb.append("\\\"");
                    break;
                case '\\':
                    sb.append("\\\\");
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '/':
                    sb.append("\\/");
                    break;
                default:
                    if((ch>='\u0000' && ch<='\u001F') || (ch>='\u007F' && ch<='\u009F') || (ch>='\u2000' && ch<='\u20FF')) {
                        String ss=Integer.toHexString(ch);
                        sb.append("\\u");
                        for(int k=0;k<4-ss.length();k++) {
                            sb.append('0');
                        }
                        sb.append(ss.toUpperCase());
                    } else {
                        sb.append(ch);
                    }
            }
        }
        return sb.toString();
    }

    protected static String jsonize(ObjectEntity map) {
        String result = "{\"className\":\"" + map.className + "\",\"version\":" +
            (map.version == null ? "null" : "\"" + map.version + "\"") + ",\"fields\":{";
        for (ObjectField of : map.fields) {
            result += "\"" + of.name + "\":{\"type\":\"" + of.type + "\",\"value\":" +
                (of.value == null ? "null" : "\"" + escapeString(of.value.toString()) + "\"") + "},";
        }
        result = result.replaceFirst(",$","");
        result += "},\"entities\":{";
        for (ObjectEntity oe : map.entities) {
            result += "\"" + oe.name + "\":" + jsonize(oe) + ",";
        }
        result = result.replaceFirst(",$","");
        result += "}}";
        return result;
    }

    public static String serialize(Object obj) {
        return jsonize(map(obj));
    }

    protected static HashMap<String, String> extractJSONFields(String jsonString) throws WrongJSONException {
        HashMap<String, String> result = new HashMap<String, String>();
        if (jsonString.charAt(0) != '{' || jsonString.charAt(jsonString.length()-1) != '}') {
            throw new WrongJSONException("Missed { or }");
        }
        jsonString = jsonString.substring(1, jsonString.length()-1);
        while(!jsonString.isEmpty()) {
            int endOfNamePos = jsonString.indexOf(':');
            String name = jsonString.substring(0, endOfNamePos-1);
            name = name.replaceFirst("\"$", "");
            name = name.replaceFirst("^\"", "");
            String value;
            int position;
            if (endOfNamePos + 1 >= jsonString.length()) {
                throw new WrongJSONException("Missed value for key");
            }
            if (jsonString.charAt(endOfNamePos + 1) == '{') {
                int prefix = 1;
                position = endOfNamePos + 2;
                while (prefix > 0) {
                    if (position > jsonString.length()) {
                        throw new WrongJSONException("Wrong structure of {}-blocks");
                    }
                    if (jsonString.charAt(position) == '{') prefix++;
                    if (jsonString.charAt(position) == '}') prefix--;
                    position++;
                }
                value = jsonString.substring(endOfNamePos + 1, position);
            } else {
                int endOfValuePos = jsonString.indexOf(',');
                position = (endOfValuePos < 0) ? jsonString.length() : endOfValuePos;
                value = jsonString.substring(endOfNamePos + 1, position);
                if (value.equals("null")) {
                    value = null;
                } else {
                    value = value.replaceFirst("\"$", "");
                    value = value.replaceFirst("^\"", "");
                }
            }
            jsonString = jsonString.length() - 1 <= position + 2 ? "" : jsonString.substring(position + 2);
            result.put(name, value);
        }
        return result;
    }

    protected static ObjectEntity unjsonize(String jsonString) throws WrongJSONException {
        ObjectEntity result = new ObjectEntity();
        HashMap<String, String> extractedEntity = extractJSONFields(jsonString);
        result.name = extractedEntity.get("name");
        result.version = extractedEntity.get("version") == null ? null : Long.parseLong(extractedEntity.get("version"));
        result.className = extractedEntity.get("className");
        result.fields = new ArrayList<ObjectField>();
        String fields = extractedEntity.get("fields") != null ? extractedEntity.get("fields") : "{}";
        HashMap<String, String> extractedFields = extractJSONFields(fields);
        for(Map.Entry<String, String> e : extractedFields.entrySet()) {
            HashMap<String, String> simpleFieldConfig = extractJSONFields(e.getValue());
            ObjectField of = new ObjectField();
            of.name = e.getKey();
            of.value = simpleFieldConfig.get("value");
            of.type = simpleFieldConfig.get("type");
            result.fields.add(of);
        }
        result.entities = new ArrayList<ObjectEntity>();
        String entities = extractedEntity.get("entities") != null ? extractedEntity.get("entities") : "{}" ;
        HashMap<String, String> extractedEntities = extractJSONFields(entities);
        for(Map.Entry<String, String> e : extractedEntities.entrySet()) {
            ObjectEntity oe = unjsonize(e.getValue());
            oe.name = e.getKey();
            result.entities.add(oe);
        }
        return result;
    }

    protected static Object unMap(ObjectEntity map) throws UnMapException {
        Object result;
        if (map.className == null) {
            throw new UnMapException("Classname missed");
        }
        try {
            if (map.version != null && map.version != ObjectStreamClass.lookup(Class.forName(map.className)).getSerialVersionUID()) {
                throw new UnMapException("Class version mismatched");
            };
            result = Class.forName(map.className).getConstructor().newInstance();
            for (ObjectField of: map.fields) {
                Field field = Class.forName(map.className).getDeclaredField(of.name);
                boolean accessible = field.isAccessible();
                if (!accessible) field.setAccessible(true);
                String typeName = field.getType().getName();
                if (!typeName.equals(of.type)) {
                    throw new UnMapException("Wrong simple type");
                }
                if (String.class.isAssignableFrom(field.getType())) {
                    field.set(result, field.getType().cast(of.value.toString()));
                } else if (Enum.class.isAssignableFrom(field.getType())) {
                    Method valueOf = field.getType().getDeclaredMethod("valueOf", String.class);
                    Object enumValue = valueOf.invoke(null, of.value.toString());
                    field.set(result, enumValue);
                } else {
                    switch (field.getType().getName()) {
                        case "boolean":
                            field.set(result, Boolean.parseBoolean(of.value.toString()));
                            break;
                        case "char":
                            field.set(result, of.value.toString().isEmpty() ? null : of.value.toString().charAt(0));
                            break;
                        case "byte":
                            field.set(result, Byte.parseByte(of.value.toString()));
                            break;
                        case "short":
                            field.set(result, Short.parseShort(of.value.toString()));
                            break;
                        case "int":
                            field.set(result, Integer.parseInt(of.value.toString()));
                            break;
                        case "long":
                            field.set(result, Long.parseLong(of.value.toString()));
                            break;
                        case "float":
                            field.set(result, Float.parseFloat(of.value.toString()));
                            break;
                        case "double":
                            field.set(result, Double.parseDouble(of.value.toString()));
                            break;
                        case "void":
                            field.set(result, null);
                            break;
                        default:
                            if (of.value == null) {
                                field.set(result, null);
                            } else {
                                throw new UnMapException("No converters for specific type");
                            }
                    }
                }
                if (!accessible) field.setAccessible(accessible);
            }
            for (ObjectEntity oe: map.entities) {
                Field field = Class.forName(map.className).getDeclaredField(oe.name);
                boolean accessible = field.isAccessible();
                if (!accessible) field.setAccessible(true);
                if (!field.getType().isAssignableFrom(Class.forName(oe.className))) {
                    throw new UnMapException("Wrong complex type");
                }
                field.set(result, unMap(oe));
                if (!accessible) field.setAccessible(accessible);
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
            NoSuchMethodException | ClassNotFoundException | NoSuchFieldException e) {
            throw new UnMapException(e.getMessage());
        }

        return result;
    }

    public static Object unserialize(String jsonString) throws WrongJSONException, UnMapException {
        return unMap(unjsonize(jsonString));
    }

}
