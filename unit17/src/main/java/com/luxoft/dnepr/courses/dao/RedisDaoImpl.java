package com.luxoft.dnepr.courses.dao;


import com.luxoft.dnepr.courses.model.Entity;
import com.luxoft.dnepr.courses.model.Redis;
import com.luxoft.dnepr.courses.model.RedisMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;


public class RedisDaoImpl extends AbstractDao<Redis> {

    @Autowired
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public Redis save(Redis redis) {
        String SQL = "insert into redis (weight) values (?)";

        jdbcTemplateObject.update( SQL, redis.getWeight());
        System.out.println("Created Record Name = " +redis.getWeight());
        return redis;
    }
    @Override
    public Redis update(Redis redis){
        String SQL = "update redis set weight = ? where id = ?";
        jdbcTemplateObject.update(SQL, redis.getWeight(), redis.getId());
        System.out.println("Updated Record with ID = " +  redis.getId() );
        return redis;
    }

    @Override
    public Redis get(long id){
        String SQL = "select * from redis where id = ?";
        Redis redis = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new RedisMapper());
        return redis;
    }

    @Override
    public boolean delete(long id) {
        String SQL = "delete from redis where id = ?";
        jdbcTemplateObject.update(SQL, id);
        System.out.println("Deleted Record with ID = " + id );
        return true;
    }

    @Override
    public List<Redis> getAll() {
        String SQL = "select * from redis";
        List <Redis> redisList = jdbcTemplateObject.query(SQL,
                new RedisMapper());
        return redisList;
    }
}