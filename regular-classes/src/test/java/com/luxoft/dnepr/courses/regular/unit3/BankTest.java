package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/7/13
 * Time: 12:50 AM
 * To change this template use File | Settings | File Templates.
 */
public class BankTest {
    User user1;
    User user2;
    User user3;
    User user4;
    User user5;
    User user6;
    Bank b;
    @Before
    public void initialize() {

        Wallet wallet1 = new Wallet();
        wallet1.setStatus(WalletStatus.BLOCKED);
        wallet1.setMaxAmount(new BigDecimal("20.5656"));
        wallet1.setId(1l);
        wallet1.setAmount(new BigDecimal("19.85868"));
        user1 = new User();
        user1.setId(1l);
        user1.setName("Vova");
        user1.setWallet(wallet1);

        Wallet wallet2 = new Wallet();
        wallet2.setStatus(WalletStatus.BLOCKED);
        wallet2.setMaxAmount(new BigDecimal("100.348927"));
        wallet2.setId(2l);
        wallet2.setAmount(new BigDecimal("34.5656"));
        user2=new User();
        user2.setId(2l);
        user2.setName("Petya");
        user2.setWallet(wallet2);

        Wallet wallet3 = new Wallet();
        wallet3.setStatus(WalletStatus.ACTIVE);
        wallet3.setMaxAmount(new BigDecimal("20.5656"));
        wallet3.setId(3l);
        wallet3.setAmount(new BigDecimal("19.85868"));
        user3 = new User();
        user3.setId(3l);
        user3.setName("Vova");
        user3.setWallet(wallet3);

        Wallet wallet4 = new Wallet();
        wallet4.setStatus(WalletStatus.ACTIVE);
        wallet4.setMaxAmount(new BigDecimal("20.5656"));
        wallet4.setId(3l);
        wallet4.setAmount(new BigDecimal("19.85868"));
        user4 = new User();
        user4.setId(4l);
        user4.setName("Vova");
        user4.setWallet(wallet4);

        Wallet wallet5 = new Wallet();
        wallet5.setStatus(WalletStatus.ACTIVE);
        wallet5.setMaxAmount(new BigDecimal("20"));
        wallet5.setId(5l);
        wallet5.setAmount(new BigDecimal("10"));
        user5 = new User();
        user5.setId(5l);
        user5.setName("Katya");
        user5.setWallet(wallet5);

        Wallet wallet6 = new Wallet();
        wallet6.setStatus(WalletStatus.ACTIVE);
        wallet6.setMaxAmount(new BigDecimal("30"));
        wallet6.setId(6l);
        wallet6.setAmount(new BigDecimal("10"));
        user6 = new User();
        user6.setId(6l);
        user6.setName("Ilya");
        user6.setWallet(wallet6);
    }

    @Test(expected = IllegalJavaVersionError.class)
    public void testMakeMoneyTransaction() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_46");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(1l,user1);
        b.setUsers(map);
        b.makeMoneyTransaction(1l,2l,new BigDecimal("23"));
    }

    @Test(expected = NoUserFoundException.class)
    public void testMakeMoneyTransaction2() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(1l,user1);
        b.setUsers(map);
        b.makeMoneyTransaction(1l,33l,new BigDecimal("23"));
    }

    @Test(expected = NoUserFoundException.class)
    public void testMakeMoneyTransaction3() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(1l,user1);
        b.setUsers(map);
        b.makeMoneyTransaction(11l,1l,new BigDecimal("23"));
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransaction4() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(3l,user3);
        map.put(4l,user4);
        b.setUsers(map);
        b.makeMoneyTransaction(3l,4l,new BigDecimal("23"));
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransaction5() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(3l,user3);
        map.put(4l,user4);
        b.setUsers(map);
        b.makeMoneyTransaction(3l,4l,new BigDecimal("20"));
    }
    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransaction6() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(3l,user3);
        map.put(4l,user4);
        b.setUsers(map);
        b.makeMoneyTransaction(3l,4l,new BigDecimal("20"));
    }

    public void testMakeMoneyTransaction7() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(1l,user1);
        map.put(4l,user4);
        b.setUsers(map);
        try {
            b.makeMoneyTransaction(1l,4l,new BigDecimal("20"));

        } catch (TransactionException e) {
            assertEquals(e.getMessage(),"User Vova wallet is blocked");
        }
    }

    public void testMakeMoneyTransaction8() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(3l,user3);
        map.put(4l,user4);
        b.setUsers(map);
        try {
            b.makeMoneyTransaction(3l,4l,new BigDecimal("26.7878"));

        } catch (TransactionException e) {
            assertEquals(e.getMessage(),"User Vova has insufficient funds (19.86 < 26.79)");
        }
    }

    public void testMakeMoneyTransaction9() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(3l,user3);
        map.put(4l,user4);
        b.setUsers(map);
        try {
            b.makeMoneyTransaction(3l,4l,new BigDecimal("26.7878"));

        } catch (TransactionException e) {
            assertEquals(e.getMessage(),"User Vova wallet limit exceeded (19.86 + 26.79 > 20.57)");
        }
    }

    public void testMakeMoneyTransaction10() throws WalletIsBlockedException, TransactionException, NoUserFoundException, InsufficientWalletAmountException, LimitExceededException {
        b = new Bank("1.7.0_45");
        Map<Long, UserInterface> map = new HashMap<Long, UserInterface>();
        map.put(5l,user5);
        map.put(6l,user6);
        b.setUsers(map);
        b.makeMoneyTransaction(5l,6l,new BigDecimal("5"));
        assertEquals(5.00,user5.getWallet().getAmount());
        assertEquals(15.00,user6.getWallet().getAmount());
    }
}
