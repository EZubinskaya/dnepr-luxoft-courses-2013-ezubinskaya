package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/3/13
 * Time: 11:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class BeverageTest {
    @Test
    public void testEquals() throws Exception {
        ProductFactory productFactory = new ProductFactory();

        Beverage book = productFactory.createBeverage("code", "Cola", 10, false);
        Beverage book2 = productFactory.createBeverage("code", "Fanta", 8, false);
        assertEquals(false, book.equals(book2));

        Beverage book3 = productFactory.createBeverage("code", "Cola", 10, false);
        Beverage book4 = productFactory.createBeverage("code", "Cola", 8, false);
        assertEquals(true, book3.equals(book4));
    }
}
