package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.WinState;

public class GameController {
    final static int DECKS_COUNT = 2;
    public final static int VICTORY_SCORE = 21;
    final static int OPTIMAL_DEALER_SCORE = 17;

    private static GameController controller;

    private List<Card> dealersHand;
    private List<Card> myHand;
    public List<Card> cardPool;

    private GameController() {
        dealersHand = new ArrayList<Card>();
        myHand = new ArrayList<Card>();
        cardPool = Deck.createDeck(DECKS_COUNT);
    }

    public static GameController getInstance() {
        if (controller == null) {
            controller = new GameController();
        }
        return controller;
    }

    public void newGame() {
        newGame(new Shuffler() {

            @Override
            public void shuffle(List<Card> deck) {
                Collections.shuffle(deck);
            }
        });
    }

    /**
     * Создает новую игру.
     * - перемешивает колоду (используйте для этого shuffler.shuffle(list))
     * - раздает две карты игроку
     * - раздает одну карту диллеру.
     *
     * @param shuffler
     */
    void newGame(Shuffler shuffler) {
        myHand.clear();
        dealersHand.clear();
        shuffler.shuffle(cardPool);
        myHand.add(Deck.getCard(cardPool));
        myHand.add(Deck.getCard(cardPool));
        dealersHand.add(Deck.getCard(cardPool));
    }

    /**
     * Метод вызывается когда игрок запрашивает новую карту.
     * - если сумма очков на руках у игрока больше максимума или колода пуста - ничего не делаем
     * - если сумма очков меньше - раздаем игроку одну карту из коллоды.
     *
     * @return true если сумма очков у игрока меньше максимума (или равна) после всех операций и false если больше.
     */
    public boolean requestMore() {
        myHand.add(Deck.getCard(cardPool));
        int myHandScore = Deck.costOf(myHand);
        return myHandScore <= VICTORY_SCORE;
    }

    /**
     * Вызывается когда игрок получил все карты и хочет чтобы играло казино (диллер).
     * Сдаем диллеру карты пока у диллера не наберется 17 очков.
     */
    public void requestStop() {
        int dealersHandScore = Deck.costOf(dealersHand);
        Card currentCard;
        while (dealersHandScore < OPTIMAL_DEALER_SCORE) {
            currentCard = Deck.getCard(cardPool);
            dealersHand.add(currentCard);
            dealersHandScore += currentCard.getCost();
        }
    }

    /**
     * Сравниваем руку диллера и руку игрока.
     * Если у игрока больше максимума - возвращаем WinState.LOOSE (игрок проиграл)
     * Если у игрока меньше чем у диллера и у диллера не перебор - возвращаем WinState.LOOSE (игрок проиграл)
     * Если очки равны - это пуш (WinState.PUSH)
     * Если у игрока больше чем у диллера и не перебор - это WinState.WIN (игрок выиграл).
     */
    public WinState getWinState() {
        int dealersScore = 0, myScore = 0;
        myScore = Deck.costOf(myHand);
        dealersScore = Deck.costOf(dealersHand);
        if (myScore > VICTORY_SCORE || myScore < dealersScore && dealersScore <= VICTORY_SCORE) {
            return WinState.LOOSE;
        }
        if (myScore > dealersScore || dealersScore > VICTORY_SCORE) {
            return WinState.WIN;
        }
        return WinState.PUSH;
    }

    /**
     * Возвращаем руку игрока
     */
    public List<Card> getMyHand() {
        return myHand;
    }

    /**
     * Возвращаем руку диллера
     */
    public List<Card> getDealersHand() {
        return dealersHand;
    }
}
