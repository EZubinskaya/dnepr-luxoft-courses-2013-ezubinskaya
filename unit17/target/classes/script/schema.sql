CREATE TABLE if not exists redis(
   ID   INT NOT NULL AUTO_INCREMENT,
   weight VARCHAR(20) NOT NULL,
   PRIMARY KEY (ID)
);

CREATE TABLE if not exists employee(
   ID   INT NOT NULL AUTO_INCREMENT,
   salary VARCHAR(20) NOT NULL,
   PRIMARY KEY (ID)
);