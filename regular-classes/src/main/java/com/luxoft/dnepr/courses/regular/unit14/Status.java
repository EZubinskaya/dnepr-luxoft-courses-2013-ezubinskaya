package com.luxoft.dnepr.courses.regular.unit14;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/22/13
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public enum Status {
    WAIT_ANSWER,
    NEXT_WORD,
    EXIT_TEST;
}
