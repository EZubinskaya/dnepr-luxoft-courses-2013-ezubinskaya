package com.luxoft.dnepr.courses.compiler;

import org.junit.Assert;

import java.io.ByteArrayOutputStream;


public abstract class AbstractCompilerTest {
	
	private static final double DELTA = .000001;
	
	/**
	 * Checks if specific string compiles to specific bytecode and produces specific result.
	 * @param expected
	 * @param input
	 */
	public static void assertCompiled(double expected, String input) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		VirtualMachine vm = VirtualMachineEmulator.create(Compiler.compile(input), out);
		vm.run();
		
		String result = new String(out.toByteArray());
		
		try {
			Assert.assertEquals("Expected [" + expected + "] but was [" + result +
                    "] when compiling input sequence: " + input,
                    expected, Double.parseDouble(result), DELTA);
		} catch (NumberFormatException ex) {
			Assert.fail("Expected [" + expected + "] but was [" + result + 
					"] when compiling input sequence: " + input);
		}
	}

	/**
	 * Checks if specific bytecode produces the expected result
	 * @param expected
	 * @param byteCode
	 */
	public static void assertBytecode(double expected, byte[] byteCode) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		VirtualMachine vm = VirtualMachineEmulator.create(byteCode, out);
		vm.run();
		
		String result = new String(out.toByteArray());
		
		try {
			Assert.assertEquals(expected, Double.parseDouble(result), DELTA);
		} catch (NumberFormatException ex) {
			Assert.fail("Expected [" + expected + "] but was [" + result + "]");
		}
	}
	
	/**
	 * Converts assembler to bytes.
	 * @param args
	 * @return bytes for assembler code
	 */
	public static byte[] asm(Object...args) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		for (int i = 0; i < args.length; i++) {
			byte arg = (Byte)args[i];
			
			if (arg == VirtualMachine.PUSH) {
				Number param = (Number)args[++i];
				Compiler.addCommand(out, arg, param.doubleValue());
			} else {
				Compiler.addCommand(out, arg);
			}
		}
		
		return out.toByteArray();
	}
}
