package com.luxoft.dnepr.courses.regular.unit6.familytree_version1.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.JSONSerialize;
import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree_version1.io.IOUtils;

import java.io.IOException;

public class FamilyTreeImpl implements FamilyTree {
	
	private static final long serialVersionUID = 3057396458981676327L;
	private Person root;
	private long creationTime;
	
	private FamilyTreeImpl(Person root, long creationTime) {
		this.root = root;
		this.creationTime = creationTime;
	}
	
	public static FamilyTree create(Person root) {
		return new FamilyTreeImpl(root, System.currentTimeMillis());
	}

    public FamilyTreeImpl() {}
	
	@Override
	public Person getRoot() {
		return root;
	}
	
	@Override
	public long getCreationTime() {
		return creationTime;
	}

    void writeObject(java.io.ObjectOutputStream out)
         throws IOException {
         IOUtils.save(out, this);

    }
    void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException, JSONSerialize.WrongJSONException, JSONSerialize.UnMapException {
         IOUtils.load(in);
    }
}
