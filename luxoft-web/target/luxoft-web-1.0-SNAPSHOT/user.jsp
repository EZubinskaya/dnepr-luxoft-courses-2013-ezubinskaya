<%--
  Created by IntelliJ IDEA.
  User: ekaterina
  Date: 12/10/13
  Time: 11:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<%
    String login = null;
    if(session.getAttribute("login") == null){
        response.sendRedirect("login");
    }else login = (String) session.getAttribute("login");
%>
<h1>Hello <%=login %>!</h1>
<a href="login">logout</a>

</body>
</html>