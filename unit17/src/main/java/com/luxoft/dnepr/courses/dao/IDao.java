package com.luxoft.dnepr.courses.dao;

import com.luxoft.dnepr.courses.model.Entity;

import java.util.List;

public interface IDao<E extends Entity> {

    E save(E e);

    E update(E e);

    E get(long id);

    boolean delete(long id);

    List<E> getAll() ;
}