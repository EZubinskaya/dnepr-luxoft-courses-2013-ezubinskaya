package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/6/13
 * Time: 8:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Wallet implements WalletInterface {

    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;
    private UserInterface owner ;

    public void setOwner(UserInterface owner) {
        this.owner = owner;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
       this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {

        if(status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(id, owner != null ? "User "+owner.getName()+" wallet is blocked" : "Wallet is blocked");
        } else if (amountToWithdraw.compareTo(amount) == 1) {
            throw new InsufficientWalletAmountException(
                    id, amountToWithdraw, amount, (owner != null ? "User "+owner.getName() : "Wallet")+
                    " has insufficient funds "+new DecimalFormat("#0.##").format(getAmount())+" < "+new DecimalFormat("#0.##").format(amountToWithdraw)
            );

        }
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        setAmount(amount.add(amountToWithdraw.negate()));
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException,  LimitExceededException {

        if(status.equals(WalletStatus.BLOCKED)) {
            throw new   WalletIsBlockedException(id, owner != null ? "User "+owner.getName()+" wallet is blocked" : "Wallet is blocked") ;
        } else if ((amountToTransfer.add(amount)).compareTo(maxAmount) == 1 ) {
            throw new  LimitExceededException(
                    id, amountToTransfer, amount, (owner != null ?  "User "+owner.getName()+" wallet" : "Wallet")+
                    " limit exceeded "+new DecimalFormat("#0.##").format(getAmount())+" + "+new DecimalFormat("#0.##").format(amountToTransfer)+" > "+new DecimalFormat("#0.##").format(getMaxAmount())) ;
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        setAmount(amount.add(amountToTransfer));
    }
}
