package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct{

    private boolean alcogol;

    boolean isNonAlcoholic (){
        return alcogol;
    }

    void setNonAlcoholic(boolean alcogol) {
        this.alcogol = alcogol;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (alcogol ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Beverage other = (Beverage) obj;
        if (alcogol != other.alcogol)
            return false;
        return true;
    }
}
