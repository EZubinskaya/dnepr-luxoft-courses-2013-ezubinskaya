package com.luxoft.dnepr.courses.unit3.controller;

import java.util.ArrayList;
import java.util.List;
import com.luxoft.dnepr.courses.unit3.model.Card;
import com.luxoft.dnepr.courses.unit3.model.Rank;
import com.luxoft.dnepr.courses.unit3.model.Suit;

public final class Deck {

    final static int MAX_DECKS = 10;
    final static int MIN_DECKS = 1;

	public static List<Card> createDeck(int size) {
        size = Math.min(Math.max(size, MIN_DECKS), MAX_DECKS);
        List<Card> cards = new ArrayList<Card>();
        while (size-- > 0) {
            for(Suit suit : Suit.values()) {
                for(Rank rank : Rank.values()) {
                    cards.add(new Card(rank, suit));
                }
            }
        }
        return cards;
	}

	public static int costOf(List<Card> hand) {
        int totalCost = 0;
        for(Card card : hand) {
            totalCost += card.getCost();
        }
        return totalCost;
	}

    public static Card getCard(List<Card> card) {
        return card.remove(0);
    }
}
