$(function() {
   var errorCookieMsg = $.cookie('error');
   if (typeof errorCookieMsg !== "undefined") {
       $("#login-error").text(errorCookieMsg);
       $.removeCookie('error');
   }
});
