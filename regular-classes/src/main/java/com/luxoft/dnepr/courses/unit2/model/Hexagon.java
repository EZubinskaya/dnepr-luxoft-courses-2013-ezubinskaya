package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: Cooper
 * Date: 12.10.13
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */
public class Hexagon extends Figure {
    double side;

    public Hexagon(double side) throws RuntimeException {
        if (side < 0) {
            throw new RuntimeException("length is less than zero");
        }
        this.side = side;
    }

    public double calculateArea() {
        return 3 * Math.sqrt(3) * Math.pow(side, 2) / 2;
    }
}