package com.luxoft.dnepr.courses.model;

public class Employee extends Entity {

    private Integer salary;

    public Integer getSalary() { return salary; }
    public void setSalary(Integer salary) { this.salary = salary; }
}