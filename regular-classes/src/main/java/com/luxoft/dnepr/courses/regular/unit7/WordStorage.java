package com.luxoft.dnepr.courses.regular.unit7;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Represents word statistics storage.
 */
public class WordStorage {

    ConcurrentHashMap<String, AtomicLong> storage= new ConcurrentHashMap<String, AtomicLong>();

    /**
     * Saves given word and increments count of occurrences.
     * @param word
     */
    public void save(String word) {
        AtomicLong value = storage.get(word);
        if (value == null){
            value = storage.putIfAbsent(word, new AtomicLong(1));
        }
        if(value != null){
            value.incrementAndGet();
        }
    }

    /**
     * @return unmodifiable map containing words and corresponding counts of occurrences.
     */
    public Map<String, ? extends Number> getWordStatistics() {
        return Collections.unmodifiableMap(storage);
    }
}
