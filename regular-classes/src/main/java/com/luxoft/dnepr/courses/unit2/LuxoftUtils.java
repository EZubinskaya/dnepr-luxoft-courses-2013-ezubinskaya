package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;


import java.text.BreakIterator;
import java.util.*;
import java.util.Map.Entry;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    public static String[] sortArray(String[] array, boolean asc) {
        String[] helpArray = array.clone();
        String temp;
        String[] result = mergesort(helpArray, 0, helpArray.length);
        if (asc) {
            return result;
        } else {
            for (int i = 0; i < result.length / 2; i++) {

                temp = result[i];
                result[i] = result[result.length - 1 - i];
                result[result.length - 1 - i] = temp;
            }
            return result;
        }

    }

    private static String[] mergesort(String[] data, int first, int last) {
        int n1 = 0;
        int n2 = 0;

        if (last > 1) {
            n1 = last / 2;
            n2 = last - n1;

            mergesort(data, first, n1);
            mergesort(data, first + n1, n2);
        }

        String[] mergeArray = merge(data, first, n1, n2);
        return mergeArray;

    }

    private static String[] merge(String[] data, int first, int n1, int n2) {
        String[] temp = new String[n1 + n2];
        int left = 0;
        int middle = 0;
        int right = 0;

        while ((middle < n1) && (right < n2)) {
            if (data[first + middle].compareTo(data[first + n1 + right]) < 0)
                temp[left++] = data[first + (middle++)];
            else
                temp[left++] = data[first + n1 + (right++)];
        }

        while (middle < n1)
            temp[left++] = data[first + (middle++)];
        while (right < n2)
            temp[left++] = data[first + n1 + (right++)];

        for (int i = 0; i < left; i++) {
            data[first + i] = temp[i];
        }

        return temp;
    }

    public static double wordAverageLength(String str) {
        String[] s = str.split(" ");
        int word = 0;
        for (int i = 0; i < s.length; i++) {
            word += s[i].length();
        }
        return (double) word / s.length;
    }

    public static String reverseWords(String str) {
        BreakIterator wordIt = BreakIterator.getWordInstance();
        wordIt.setText(str);
        int start = wordIt.first();
        StringBuilder currentWord;
        StringBuilder outWord = new StringBuilder();
        for (int end = wordIt.next(); end != BreakIterator.DONE; start = end, end = wordIt
                .next()) {
            currentWord = new StringBuilder(str.substring(start, end));
            outWord.append(currentWord.reverse());
        }

        return outWord.toString();
    }

    public static char[] getCharEntries(String str) {
        Map<Character, Integer> hm = new HashMap<Character, Integer>();
        Character a;
        String strWithoughtSpace = str.replaceAll(" ", "");
        for (int i = 0; i < strWithoughtSpace.length(); i++) {
            a = strWithoughtSpace.charAt(i);
            Integer b = hm.get(a);

            if (b == null) {
                hm.put(a, 1);

            } else {
                hm.put(a, b + 1);
            }
        }

        List<Entry<Character, Integer>> entries = new ArrayList<Entry<Character, Integer>>(
                hm.entrySet());
        Collections.sort(entries, new Comparator<Entry<Character, Integer>>() {
            public int compare(Entry<Character, Integer> e1,
                               Entry<Character, Integer> e2) {
                if (e1.getValue() > e2.getValue()) {
                    return -1;
                } else if (e1.getValue() == e2.getValue()) {
                    return e1.getKey().compareTo(e2.getKey());
                } else {
                    return 1;
                }
            }

        });


        Map<Character, Integer> orderedMap = new LinkedHashMap<Character, Integer>();
        for (Entry<Character, Integer> entry : entries) {
            orderedMap.put(entry.getKey(), entry.getValue());
        }

        Character[] strArr = orderedMap.keySet().toArray(new Character[orderedMap.keySet().size()]);
        char[] ch = new char[strArr.length];
        for (int j = 0; j < strArr.length; j++) {
            ch[j] = strArr[j];
        }
        return ch;
    }

    public static double calculateOverallArea(List<Figure> figures) {
        double area = 0;
        Iterator<Figure> it = figures.iterator();
        while (it.hasNext()) {
            try {
                Figure f = it.next();
                area += f.calculateArea();
            } catch (RuntimeException ex) {
                System.out.println(ex.getMessage());
            }

        }
        return area;
    }
}