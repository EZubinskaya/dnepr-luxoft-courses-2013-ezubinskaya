package com.luxoft.dnepr.courses.regular.unit3;

public enum WalletStatus {

    ACTIVE, BLOCKED;

}
