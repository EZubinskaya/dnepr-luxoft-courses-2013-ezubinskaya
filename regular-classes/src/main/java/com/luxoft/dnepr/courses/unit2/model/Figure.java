package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created with IntelliJ IDEA.
 * User: Cooper
 * Date: 12.10.13
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */
public abstract class Figure {

    public abstract double calculateArea();
}
