package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {
	
	public static final boolean DEBUG = true;

	public static void main(String[] args) {
		byte[] byteCode = compile(getInputString(args));
		VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
		vm.run();
	}

	static byte[] compile(String input) {
        if(input == "") {
            throw new CompilationException("You write empty string");
        }
        input = input.replaceAll("\\s+","");
        byte[] result;
        ArrayList<Token> tokens = new ArrayList<Token>();
        ExpressionMatcher m = new ExpressionMatcher(input);
        String partOfExpr;
        Operator operator;
        Token token = null;
        double numberInExpr;
        while(m.find())
        {
            partOfExpr = m.getFounded();
            System.out.println("partOfExpr " + partOfExpr);
            if (Operator.contains(partOfExpr)) {
                operator = Operator.parseString(partOfExpr);
                if (operator != Operator.MINUS && operator != Operator.PLUS) {
                    throw new CompilationException("Wrong operator's syntax");
                }
                token = new Token(0.0, operator, operator.getPriority() + m.getCurrentPrioity());
            } else {
                numberInExpr = Double.parseDouble(partOfExpr);
                if (m.find()) {
                    try {
                        operator = Operator.parseString(m.getFounded());
                    } catch (IllegalArgumentException ex) {
                        throw new CompilationException("Input string must not contain two numbers in a row");
                    }
                    token = new Token(numberInExpr, operator, operator.getPriority() + m.getCurrentPrioity());
                } else {
                    token = new Token(numberInExpr, Operator.VOID, m.getCurrentPrioity());
                }
            }
            tokens.add(token);
        }
        if (tokens.size() > 0 && token.getOperator() != Operator.VOID) {
            throw new CompilationException("Input string must not end with a operator");
        }
        if(m.getBracket() != 0) {
            throw new CompilationException("Forget close or open bracket");
        }
        while(tokens.size() > 1) {
            Token firstToken = Collections.max(tokens);
            int tokenIndex = tokens.indexOf(firstToken);
            tokens.get(tokenIndex+1).mergeToken(firstToken);
            tokens.remove(tokenIndex);
        }
        if (tokens.size() == 1) {
            result = tokens.get(0).prepareForOutput().getStack().toByteArray();
        } else {
            result = new byte[0];
        }
		return result;
	}

	/**
	 * Adds specific command to the byte stream.
	 * @param result
	 * @param command
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command) {
		result.write(command);
	}

	/**
	 * Adds specific command with double parameter to the byte stream.
	 * @param result
	 * @param command
	 * @param value
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
		result.write(command);
		writeDouble(result, value);
	}

	private static void writeDouble(ByteArrayOutputStream result, double val) {
		long bits = Double.doubleToLongBits(val);
		
		result.write((byte)(bits >>> 56));
		result.write((byte)(bits >>> 48));
		result.write((byte)(bits >>> 40));
		result.write((byte)(bits >>> 32));
		result.write((byte)(bits >>> 24));
		result.write((byte)(bits >>> 16));
		result.write((byte)(bits >>>  8));
		result.write((byte)(bits >>>  0));
	}
	
	private static String getInputString(String[] args) {
		if (args.length > 0) {
			return join(Arrays.asList(args));
		}
		Scanner scanner = new Scanner(System.in);
		List<String> data = new ArrayList<String>();
//		while (scanner.hasNext()) {
//            data.add(scanner.next());
//		}
        while (!scanner.hasNext()) {}
        data.add(scanner.next());
		return join(data);
	}

	private static String join(List<String> list) {
		StringBuilder result = new StringBuilder();
		for (String element : list) {
			result.append(element);
		}
		return result.toString();
	}

}
