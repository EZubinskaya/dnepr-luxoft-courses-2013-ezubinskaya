package com.luxoft.dnepr.courses.regular.unit12;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 12/11/13
 * Time: 8:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoginServlet extends HttpServlet{

    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        if(session != null){
            session.invalidate();
        }
        response.sendRedirect("index.html");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        HashMap<String,String> data=(HashMap<String,String>)getServletContext().getAttribute("Database");
        String username = request.getParameter("login");
        String password = request.getParameter("password");
        if (data.containsKey(username) == true && data.get(username).equals(password)) {
            HttpSession session = request.getSession();
            session.setAttribute("login",username);
            //setting session to expiry in 1 mins
            session.setMaxInactiveInterval(1*60);
            Cookie userName = new Cookie("login", username);
            userName.setMaxAge(1*60);
            response.addCookie(userName);
            response.sendRedirect("user");
        } else {
            Cookie error = new Cookie("error", "Wrong login");
            error.setMaxAge(-1);
            response.addCookie(error);
            response.sendRedirect("index.html");
        }
    }
}
