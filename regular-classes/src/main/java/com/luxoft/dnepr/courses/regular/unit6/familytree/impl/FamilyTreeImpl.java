package com.luxoft.dnepr.courses.regular.unit6.familytree.impl;

import com.luxoft.dnepr.courses.regular.unit6.familytree.FamilyTree;
import com.luxoft.dnepr.courses.regular.unit6.familytree.Person;
import com.luxoft.dnepr.courses.regular.unit6.familytree.io.IOUtils;

import java.io.IOException;

public class FamilyTreeImpl implements FamilyTree {
	
	private static final long serialVersionUID = 3057396458981676327L;
	private Person root;
	private transient long creationTime;
	
	private FamilyTreeImpl(Person root, long creationTime) {
		this.root = root;
		this.creationTime = creationTime;
	}
	
	public static FamilyTree create(Person root) {
		return new FamilyTreeImpl(root, System.currentTimeMillis());
	}

    public FamilyTreeImpl() {}
	
	@Override
	public Person getRoot() {
		return root;
	}
	
	@Override
	public long getCreationTime() {
		return creationTime;
	}

    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
         IOUtils.save(out, this);

    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, PersonImpl.PersonJSONParseException {
        FamilyTree loadedFamilyTree = IOUtils.load(in);
        this.root = loadedFamilyTree.getRoot();
    }
}
