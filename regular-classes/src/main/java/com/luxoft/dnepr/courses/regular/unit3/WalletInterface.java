package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

interface WalletInterface {

    Long getId();
    void setId(Long id);
    BigDecimal getAmount();
    void setAmount(BigDecimal amount);
    WalletStatus getStatus();
    void setStatus(WalletStatus status);
    BigDecimal getMaxAmount();
    void setMaxAmount(BigDecimal maxAmount);
    void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException, LimitExceededException;
    void withdraw(BigDecimal amountToWithdraw);
    void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException, InsufficientWalletAmountException;
    void transfer(BigDecimal amountToTransfer);
    public void setOwner(UserInterface owner);
}
