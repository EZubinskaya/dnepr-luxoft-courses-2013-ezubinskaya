package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;


/**
 * Created with IntelliJ IDEA.
 * User: ekaterina
 * Date: 11/7/13
 * Time: 12:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class WalletTest {
    Wallet currentWallet;
    @Before
    public void initialize() {

    }

    @Test(expected = WalletIsBlockedException.class)
    public void testCheckWithdrawal() throws Exception {
        currentWallet =  new Wallet();
        currentWallet.setAmount(new BigDecimal("23"));
        currentWallet.setId(1l);
        currentWallet.setMaxAmount(new BigDecimal("25"));
        currentWallet.setStatus(WalletStatus.BLOCKED);
        currentWallet.checkWithdrawal(new BigDecimal("30"));
    }
    @Test(expected = InsufficientWalletAmountException.class)
    public void testCheckWithdrawal2() throws Exception {
        currentWallet =  new Wallet();
        currentWallet.setAmount(new BigDecimal("23"));
        currentWallet.setId(1l);
        currentWallet.setMaxAmount(new BigDecimal("25"));
        currentWallet.setStatus(WalletStatus.ACTIVE);
        currentWallet.checkWithdrawal(new BigDecimal("300"));
    }
    @Test
    public void testWithdraw() throws Exception {
        currentWallet =  new Wallet();
        currentWallet.setAmount(new BigDecimal("23"));
        currentWallet.setId(1l);
        currentWallet.setMaxAmount(new BigDecimal("25"));
        currentWallet.setStatus(WalletStatus.ACTIVE);
        currentWallet.withdraw(new BigDecimal("10"));
        assertEquals(new BigDecimal("13"), currentWallet.getAmount());

    }

    @Test (expected = WalletIsBlockedException.class)
    public void testCheckTransfer() throws Exception {
        currentWallet =  new Wallet();
        currentWallet.setAmount(new BigDecimal("23"));
        currentWallet.setId(1l);
        currentWallet.setMaxAmount(new BigDecimal("25"));
        currentWallet.setStatus(WalletStatus.BLOCKED);
        currentWallet.checkTransfer(new BigDecimal("30"));
    }

    @Test (expected = LimitExceededException.class)
    public void testCheckTransfer2() throws Exception {
        currentWallet =  new Wallet();
        currentWallet.setAmount(new BigDecimal("23"));
        currentWallet.setId(1l);
        currentWallet.setMaxAmount(new BigDecimal("35"));
        currentWallet.setStatus(WalletStatus.ACTIVE);
        currentWallet.checkTransfer(new BigDecimal("13"));
    }

    @Test
    public void testTransfer() throws Exception {
        currentWallet =  new Wallet();
        currentWallet.setAmount(new BigDecimal("34.5656"));
        currentWallet.setId(1l);
        currentWallet.setMaxAmount(new BigDecimal("35"));
        currentWallet.setStatus(WalletStatus.ACTIVE);
        currentWallet.transfer(new BigDecimal("16"));
        assertEquals(new BigDecimal("50.5656"), currentWallet.getAmount());
    }
}
